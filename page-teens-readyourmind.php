<?php
/*
Template Name: Teens Read Your Mind
*/
get_header('teens'); ?>

<div id="middle" style="overflow: hidden;">

    <!-- use for no sidebar <div id="main" style="width: 92%;"> -->
    <div id="main" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post();

    ?>


	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>


    <div class="meta">

	<p class="breadcrumbs"><b>You are Here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>
    <?php endwhile; endif; ?>



    </div>


    <?php get_sidebar('teens-read-your-mind'); ?>


</div>

<?php get_footer('teens'); ?>