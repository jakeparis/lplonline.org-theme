<?php

    $importantMsg = get_option( 'lpl-important-message' );
    $displayedMessage = '';
	if( $importantMsg ) {
    	$displayMessage = true;
    	$imgMsgExp = get_option('lpl-important-message-expiration');
    	if( $imgMsgExp ) {
    		$now = new DateTime('now', new DateTimeZone('America/New_York'));
    		$imgMsgExp = new DateTime($imgMsgExp, new DateTimeZone('America/New_York'));
			
    		if( $now->format('Ymd') > $imgMsgExp->format('Ymd') )
    			$displayMessage = false;
    	}

		$displayedMessage = "<div id='importantMsg'><img src=\"" . THEMEDIR . "/i/warning-icon-black.png\" alt=\"Important\" width=\"50px\" /> $importantMsg</div>";

    	// if( $displayMessage )
		// 	$hashedMsg = md5($importantMsg);
		// 	if($_COOKIE['seen-important-message'] != $hashedMsg){
		// 		setcookie('seen-important-message',$hashedMsg,0,'/');
		// 	}
	    	
    }
?>
<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/Organization">

<head>

	<link rel="preconnect" href="//fonts.googleapis.com">
	<link rel="preconnect" href="//fonts.gstatic.com" crossorigin>

	<link href="//fonts.googleapis.com/css2?family=Lora:ital,wght@0,400;0,700;1,400;1,700&family=Prompt:ital,wght@0,200;0,400;0,700;1,400;1,700&family=Shadows+Into+Light+Two&display=swap" rel="stylesheet">

    <meta charset="<?php  bloginfo('charset'); ?>">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php wp_title(' // ',1,'right'); bloginfo('name'); ?></title>

   <script type="text/javascript">
    var THEMEDIR = '<?php echo THEMEDIR; ?>';
    var SITEHOME = '<?php echo SITEHOME; ?>';
   </script>

   <?php wp_head(); ?>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-77TD97NK6Z"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-77TD97NK6Z');
</script>
</head>

<body class="<?php if(is_front_page()) echo 'home '; else echo 'nothome '?>">
<a class="skip-link screen-reader-text" href="#<?php if(is_front_page()) echo 'content'; else echo 'middle'?>">Skip to content</a>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="container">
	<?php echo $displayedMessage ?>
<header>
	
    <a href="<?php echo SITEHOME; ?>">
    	<img src="<?php echo THEMEDIR; ?>/i/logo-2021.png" id="logo" alt="Lewiston Public Library">
    	<img id="logo-thumb" src="<?php echo THEMEDIR; ?>/i/logo-2021.png" alt="Lewiston Public Library">
	</a>

	<a id="mobile-menu" onclick="toggleEl('#mainNav')">
		<img id="mobile-menu-icon" src="<?php echo THEMEDIR; ?>/i/menu-icon.png?version=<?php echo date('Y-m-d-h-i-s') ?>" alt="Toggle Menu">
	</a>

	<a id="mobile-search-link" onclick="toggleEl('#topSearch')">
		<img id="mobile-search-icon" src="<?php echo THEMEDIR; ?>/i/search-icon.png?version=<?php echo date('Y-m-d-h-i-s') ?>" alt="Toggle Search">
	</a>

	<a id="mobile-map-link" href="<?php echo get_permalink( 14 ); ?>">
		<img id="mobile-map-icon" src="<?php echo THEMEDIR; ?>/i/map-pin-icon.png?version=<?php echo date('Y-m-d-h-i-s') ?>" alt="Contact">
	</a>

	<a id="mobile-account-link" href="<?php echo get_permalink( 20 ); ?>">
		<img id="mobile-account-icon" src="<?php echo THEMEDIR; ?>/i/account-icon.png?version=<?php echo date('Y-m-d-h-i-s') ?>" alt="Login to your account">
	</a>

	<script>
		function toggleEl(selector){
			$ = jQuery;
			
			if($(selector).css('display') == 'block'){
				$(selector).slideUp();
			} else {
				$(selector).slideDown();
			}
			
		}
	</script>
	
    <div id="topLinks">
	<a href="<?php echo get_permalink( 20 ); ?>">Login</a>
	<a href="<?php echo get_permalink( 14 ); ?>">Contact Us</a>
	<a href="http://minerva.maine.edu/search~S19/" class="show-800-up">Library Catalog</a>
	<a href="http://m.minerva.maine.edu/search~S19/" class="hide-800-up">Library Catalog</a>
	<a href="/services/library-cards/">Library Cards</a>
    </div>

    <div id="topSearch">
    	<?php
		if( class_exists('LibraryMultiSearch') ) {
			echo LibraryMultiSearch::doSearchBox();
		} else {
    		?>
			<form method="get" class="lookForPrefill" action="<?php echo SITEHOME; ?>">
				<input type="hidden" name="viewportWidth" value="" />
			    <input name="s" data-prefill="Search for..." type="text" value="<?php the_search_query(); ?>" >
			    <button type="submit" name="searchbutton" value=""></button>
			    <p id="openSearchFeatures"><a href="#searchFeatures" >Search Features</a></p>
			    <div id="searchFeatures">

				<p id="searchWhereWrap">
				    <b>Search the</b> &nbsp;
				    <label class="hilite"><input type="radio" name="lookIn[]" checked value="catalog"> Catalog</label>
				    <label><input type="radio" name="lookIn[]" value="website"> Website</label>
				</p>

				<div id="catalogSearchFeatures">
				    <p id="searchTypesWrap">
					<b>Search For</b> &nbsp;
					<label class="hilite"><input id="catalogSearchAllTypes" type="checkbox" name="mat[]" value="all" checked="checked"> All types</label>
					<label><input type="checkbox" name="mat[]" value="x"> eBooks</label>
					<label><input type="checkbox" name="mat[]" value="5"> DVDs</label>
					<label><input type="checkbox" name="mat[]" value="1"> Music</label>
					<label><input type="checkbox" name="mat[]" value="3"> Audiobook CDs</label>
					<label><input type="checkbox" name="mat[]" value="a"> Print books</label>
				    </p>

				    <p id="searchScopeWrap">
					<b>Which Libraries?</b> &nbsp;
					<label class="hilite"><input type="radio" name="scope" value="19" checked="checked"> Lewiston</label>
					<label><input type="radio" name="scope" value="81"> Lewiston Childrens</label>
					<label><input type="radio" name="scope" value="2"> Auburn</label>
					<label><input type="radio" name="scope" value="71"> All Maine Libraries</label>
				    </p>

				    <p id="searchAvailLimit">
					<b>Show</b>  &nbsp;
					<label class="hilite"><input type="radio" name="availlim" value="0" checked="checked"> Everything</label>
					<label><input type="radio" name="availlim" value="1"> Currently On Shelf</label>
				    </p>

				    <p id="searchFieldsWrap">
					<b>Search Within</b> &nbsp;
					<label class="hilite"><input id="catalogSearchAllFields" type="checkbox" name="searchFields[]" value="all" checked="checked"> Everything</label>
					<label><input type="checkbox" name="searchFields[]" value="t"> Titles</label>
					<label><input type="checkbox" name="searchFields[]" value="a"> Authors</label>
					<label><input type="checkbox" name="searchFields[]" value="d"> Subjects</label>
					<label><input type="checkbox" name="searchFields[]" value="N"> Item Notes</label>
				    </p>

				</div>

				<p><input type="submit" value="Search"> &nbsp;&nbsp; <a href="#" id="hideSearchFeatures">cancel</a></p>

			    </div>
			</form>
	
	<?php } ?>


	<a id="hideAllMenusButton" href="#mainNav" data-state="hidden">show all navigation menus</a>
		
	    <p id="topContactInfo">
				Monday-Thursday: 9:30AM - 7PM
				<span> &bull; </span>
				<!-- Friday & Saturday: 9:30AM - 1PM
				<span> &bull; </span>
				Sunday: Closed -->
		<?php if(!summerHours()) { ?>
		    Friday-Saturday : 9:30AM - 1PM
		<?php } else { ?>
		    Friday: 9:30AM - 1PM <span> &bull;</span>
		    Saturday: 9:30AM - 1PM
		<?php } ?>
		<span> &bull; </span>
		Sunday: Closed
				<br>
		200 Lisbon St, Lewiston ME 04240<span> &nbsp; &bull; &nbsp;</span>
		207-513-3004
		<br>
		    <!-- <a id="viewAuburnHours" style="font-size:.9em;top:5px;color:#666;font-style:italic;text-shadow:none;" target="_blank" class="hideFromMobile" href="http://www.auburnpubliclibrary.org/hours-2/">view Auburn Public Library's hours</a> -->
	    </p>
    </div>
			
    <div id="topSectionButtons">
		<div class="_left">
			<h5>
				<a href="http://lplonline.org/kids/">Kids</a>
			</h5>
			<h5>
				<a href="http://lplonline.org/teens/">Teens</a>
			</h5>
			<h5>
				<a href="http://libraryla.org" target="libraryla">Online Services</a>
			</h5>
		</div>

		<div class="_right">
			<h5>
				<a href="https://www.maine.gov/governor/mills/lewiston" target="_blank">Healing Together Resources</a>
			</h5>
		</div>

    </div>


</header>