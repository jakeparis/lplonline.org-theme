<div role="complementary" id="sidebar">
<?php

 // USE THIS FOR EVENTS
$events = mejp_getEvents(3, 'future', 'read-your-mind');

if ($events) :

echo '<h2>Upcoming Read Your Mind Events</h2>';
$odd = '';
$shown = array();

foreach($events as $eventDate => $evsForDate) {
	foreach($evsForDate as $evOccurances) {
		foreach($evOccurances as $ev) {

			if($ev->eGroup == 1) {
			// don't show the event multiple times...
			if(in_array($ev->ID,$shown)) continue;
			}
			?>

			<div class="singleEvent" style="padding-left:5px;">
			<h2><a href="<?php echo $ev->guid; ?>"><?php echo jp_get_featured_image($ev->ID) . $ev->post_title; ?></a></h3>
			<p class="eventDate"><?php echo date('M d, Y', strtotime($eventDate)) . ' &mdash; ' . $ev->time .'</p>';

			$showChars = 150;

			$desc = strip_tags($ev->post_content);
			list($desc) = str_split($desc,$showChars);
			echo '<p>' . $desc;
			if(strlen( $ev->post_content ) > $showChars)
				echo '...<a href="' . $ev->guid . '">read more</a>.';

			echo '</p>';
			?>
			</div>


			<?php
			$odd = ($odd == 'odd') ? '' : 'odd';

			$shown[] = $ev->ID;
		}
	}
}

echo '<p style="background:transparent;text-align:center;"><b>See <a href="' . get_permalink(1162) . '">all upcoming events for teens</a>.</b></p>';

else : ?>

<p style="color: #bbb;text-align:right;font-style:italic;padding:15px 15px 0;">No events at the moment.</p>

<?php


endif;

?>

</div>