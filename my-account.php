<?php
/*
Template Name: My Account
*/

get_header();

?>


<div id="middle">

    <div id="main" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


	<h1><?php the_title(); ?></h1>

	<?php the_content(); ?>

	<form action="https://minerva.maine.edu/patroninfo" method="post" id="accountLoginForm" class="lookForPrefill pageForm">
	<label for="name">Your Name</label>
	<input type="text" id="name" name="name" value="<?php echo $_COOKIE['patronName']; ?>">
	<label for="code">Library Card Number</label>
	<input type="text" id="code" name="code" maxlength="14"
	<?php if(isset($_COOKIE['patronCardno'])) echo 'value="' . $_COOKIE['patronCardno'] . '"';
	else echo 'data-prefill="Example: 24240001234567"'; ?>>
	<input type="submit" name="submit" value="Log in" />
	</form>




    <div class="meta">

	<p class="breadcrumbs"><b>You are Here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>

    <?php endwhile; endif; ?>

    </div>


    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>