<?php
/*
Template Name: Event Listing
*/
get_header(); ?>

<div id="middle">

    <div id="main" role="main" page="event-listing">


    <?php $odd = 'odd';
    if (have_posts()) : while (have_posts()) : the_post(); ?>

	<h1><?php the_title(); ?></h1>

	<?php the_content();


    endwhile; endif;

   ?>
    </div>

    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>