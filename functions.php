<?php
add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );
add_editor_style('style-forEditor.css');

define('SITEHOME',get_bloginfo('home'));
define('THEMEDIR',get_bloginfo('template_directory'));
$uploadsDir = wp_upload_dir();
define('UPLOADSPATH',$uploadsDir['basedir']);
define('UPLOADSDIR',$uploadsDir['baseurl']);
define('DOMAIN','lplonline.org');
define('FORMRECIPIENT','lplreference@gmail.com');


// function to use featured image if available, otherwise fallback to
// attached images
function jp_get_featured_image($pID = '',$size = 'large',$attr = null) {

    // if not specified page id, use in loop
    if($pID == '') {
	global $post;
	$pID = $post->ID;
    }

    // find WP featured image
    $thumb = get_the_post_thumbnail( $pID, $size, $attr );

    // if none, try db search for image attachments
    if($thumb == '') {

	global $wpdb;
	$aID = $wpdb->get_var("SELECT ID FROM " . $wpdb->posts . " WHERE
				   post_type = 'attachment' AND
				   post_parent = '$pID'");
	if($aID != NULL) {
	    // get the thumbnail version
	    $url = wp_get_attachment_thumb_url( $aID );
	    $thumb = '<img alt="thumbnail" src="'.$url.'" />';
	} else
	    $thumb = '';
    }

    return $thumb;
}


add_action( 'init', function() {

	wp_enqueue_script('jquery');
	wp_register_script('myscripts',THEMEDIR.'/scripts.js',array('jquery'),'16.2');
	wp_enqueue_script('myscripts');

	if( ! is_admin() ) {

		wp_enqueue_style( 'lpl', 
			get_stylesheet_directory_uri() . '/style.css',
			array(
			),
			'19.8.0'
		);

		wp_enqueue_style( 'lpl-print', 
			get_stylesheet_directory_uri() . '/print.css',
			array(
				'lpl',
			),
			'1',
			'print'
		);
	}
});

function jpIfSet($postName) {

    if(isset($_POST[$postName])) echo $_POST[$postName];
}

function jpIfSel($name,$val) {
    if(isset($_POST[$name]) && $_POST[$name] == $val)
	echo 'selected="selected"';
}
function jpIfChe($name,$val) {
    if(is_array($_POST[$name])) {
	if(in_array($val,$name))
	    echo 'checked="checked"';
    } else {
	if(isset($_POST[$name]) && $_POST[$name] == $val)
	    echo 'checked="checked"';
    }
}

function jpShowCode($code,$die = false) {
    echo '<pre style="white-space:pre-wrap;">';

    if(is_array($code)) {
	print_r($code);
    } else {
	$code = str_replace(array('<','>','\n'),array('&lt;','&gt;','<br>'),$code);
	echo $code;
    }

    echo '</pre>';
    if($die != false) die;
}

function jpGetBookLetters() {
    global $wpdb;

    $letters = $wpdb->get_results(
	    "SELECT ".$wpdb->posts.".post_title,
	    ".$wpdb->posts.".ID as ID
	    FROM ".$wpdb->postmeta.", ".$wpdb->posts."
	    WHERE ".$wpdb->posts.".ID = ".$wpdb->postmeta.".post_id
	    AND meta_key = 'bookletters-code'
	    AND ".$wpdb->posts.".post_status='Publish'
	    ORDER BY post_title ASC;"
	    );
    if($letters) {
	foreach($letters as $let) {
	    $permalink = get_permalink( $let->ID );
	    $bl[] = array('permalink' => $permalink,
			  'title' => $let->post_title
			  );
	}
	return $bl;

    } else return array();

}


function addToMailingList($name,$email,$lists = array()) {
    // (MAILCHIMP)
    if( !empty($lists) ) {

	if(strpos($name,' ') !== FALSE) {

	    list($fname,$lname) = explode(' ',$regName);
	    if($fname != '')
		$merge_vars['FNAME'] = $fname;
	    if($lname != '')
		$merge_vars['LNAME'] = $lname;

	} else {
	    $merge_vars['FNAME'] = $name;
	}

	$lplLists = '';
	if( in_array( 'mailingListTeens', $lists) ) {
	    $lplLists = 'Teen Events,';

	}
	if( in_array( 'mailingListKids', $lists) ) {
	    $lplLists .= 'Children\'s Events,';

	}
	if( in_array( 'mailingListAdults', $lists) ) {
	    $lplLists .= 'Adult Events';

	}
	trim($lplLists,',');

	if($lplLists != '') {

	    $merge_vars['GROUPINGS'] = array(
		    array('name'=>'Send me...', 'groups'=> $lplLists)
	    );
	    $merge_vars['MC_NOTES'] = array(
		'note' => ' Signed up from lplonline.org contact form.'
	    );

	    $apikey = '024ebda876429ae0b47e6f81a0594eac-us6';
	    require_once(THEMEDIR.'/lib/MCAPI.class.php');
	    $api = new MCAPI($apikey);
	    $listId = 'dbbfa932a9';


	    $retval = $api->listSubscribe( $listId, $email, $merge_vars, 'html', false, true );

	    if (!$api->errorCode) {
		return 'You have been signed up for the Lewiston Public Library\'s Event Mailings (' . $lplLists . '). ';
	    } else
		return 'Error signing up';

	}

    }
}


function jp_breadcrumb($pID = '') {

    $sep = '&nbsp; &raquo; &nbsp;';

    // if not specified page id, use in loop
    if($pID == '') {
	global $post;
	$pID = $post->ID;
    }

    $ret = '<a title="go to the homepage" href="' . SITEHOME . '">Home</a>';
    global $wpdb;
    $parent = $wpdb->get_var("SELECT post_parent from " . $wpdb->posts . " WHERE ID = $pID");

    if($parent != 0 && $parent != '') {

	$pTitle = $wpdb->get_var("SELECT post_title FROM " . $wpdb->posts . " WHERE ID = $parent");

	if(strlen($pTitle) >22) {
	    list($pTitle) = str_split($pTitle,22);
	    $pTitle .= '...';
	}

	$printMe = $sep . '<a title="'.get_the_title($parent).'" href="' . get_permalink($parent) . '">'  .$pTitle . '</a>' . $printMe;

	jp_breadcrumb_chi($parent,$sep,$printMe);

    }

    $ret .= $printMe . $sep . '<i>' . get_the_title($pID) . '</i>';
    return $ret;
}

function jp_breadcrumb_chi($pID,$sep,&$ref) {
	global $wpdb;

	$parent = $wpdb->get_var("SELECT post_parent from " . $wpdb->posts . " WHERE ID = $pID");

	if($parent != 0 && $parent != '') {

	    $pTitle = $wpdb->get_var("SELECT post_title FROM " . $wpdb->posts . " WHERE ID = $parent");

	    if(strlen($pTitle) >22) {
		list($pTitle) = str_split($pTitle,22);
		$pTitle .= '...';
	    }

	    $ref = $sep . '<a title="go to the '.get_the_title($parent).' page" href="' . get_permalink($parent) . '">'  .$pTitle . '</a>' . $ref;

	    jp_breadcrumb_chi($parent,$sep,$ref);

	}

}


function findHoliday($nth, $weekDay) {
	date_default_timezone_set('America/New_York');
	if($nth == 'last') {
		$oNth = $nth;
		$nth = 4; // JUST FOR NOW, WE'LL CHECK IT LATER FOR A FIFTH WEEK;
	}

	if(!is_numeric($weekDay))
		$weekDay = date('w', strtotime($weekDay));

	$monthm = date('n');
	$yeary = date('y');

	$earliestDate = 1 + 7 * ($nth -1);
	$earliestDay = date("w",mktime(0,0,0,$monthm,$earliestDate,$yeary));

	if( $weekDay == $earliestDay )
		$offset = 0;
	else {
		if( $weekDay < $earliestDay )
			$offset = $weekDay + (7 - $earliestDay);
		else
			$offset = ($weekDay + (7 - $earliestDay)) - 7;
	}

	$holidayDate = date('j',mktime(0,0,0,$monthm,$earliestDate + $offset,$yeary));

	if($oNth == 'last') { // CHECK FOR 5 WEEKS --
		$firstMonthDay = date('w', mktime(0,0,0,$monthm,1,$yeary)); // NUMBER OF FIRST DAY OF THE MONTH

		if($monthm == 5 && ($firstMonthDay == 0 || $firstMonthDay == 6 || $firstMonthDay == 1))  // CHECK MAY;
			$holidayDate = $holidayDate + 7;
	}

	return $holidayDate; // RETURNS DAY OF THE MONTH, NO LEADING ZERO;
}


function summerHours() {
	date_default_timezone_set('America/New_York');
	$month = (isset($_GET['m'])) ? $_GET['m'] : date('n');
	$date = (isset($_GET['d'])) ? $_GET['d'] : date('j');

	// BUILD HOLIDAYS ARRAY;
	$holidays['Memorial Day'] = findHoliday('last','mon');
	$holidays['Labor Day'] = findHoliday(1,'mon');


	if ($month > 5 && $month < 9) // JUNE - AUGUST
		$summerHours = TRUE;
	else if ($month == 5 && $date > $holidays['Memorial Day'])
		$summerHours = TRUE;
	else if ($month == 9 && $date < $holidays['Labor Day'])
		$summerHours = TRUE;
	else
		$summerHours = FALSE;

	return $summerHours;
  }

// for homepage banners

function resizeImage($source,$type,$nw,$nh) {
	switch($type) {
		case 'image/gif':
		$old = imagecreatefromgif($source);
		break;
		case 'image/jpeg':
		$old = imagecreatefromjpeg($source);
		break;
		case 'image/png':
		$old = imagecreatefrompng($source);
		break;
	}

	list($w,$h) = getimagesize($source);

	//if($w == 166 && $h == 70) // SIZE FOR BANNERS;
	//	return true;

	//$nw = 166;
	$calcH = $h * ($nw/$w);

	// DON'T IT MAKE LOOK TOO DUMB
	if($calcH > $nh+10) // was 80
		$calcH = $nh+5; // was 75
	else if ($calcH < $nh-10) // was 60
		$calcH = $nh-5; // was 65


	$dimg = imagecreatetruecolor($nw, $nh);
	//$white = imagecolorallocate($dimg,255,255,255); // set background to white;

	imagecopyresampled($dimg,$old,0,0,0,0,$nw,$nh,$w,$h);

	//$source = str_replace(' ','_',$source);

	if(imagejpeg($dimg,$source,100)){
		imagedestroy($dimg);
		imagedestroy($old);
		return TRUE;

	} else {
		imagedestroy($dimg);
		imagedestroy($old);
		return FALSE;
	}
}

if(!function_exists('add_homebanner_admin_page')) {

    function add_homebanner_admin_page() {
	add_submenu_page('upload.php',  'Homepage Banners Admin', 'Homepage Banners', 5, 'homepage-banners-admin-page','jp_homepage_banners_admin');
    }
}
add_action('admin_menu', 'add_homebanner_admin_page');


function jp_homepage_banners_admin() {
    global $wpdb;
    ?>
    <div id="wrap">
	    <h2>Manage Homepage Banners</h2>
	    <p>This controls the images shown under the popular pages section</p>

    <?php
    /***  ACTION SECTION ***/

    if($_GET['delete'] == 'confirmed'){
	    

	    $filename = $wpdb->get_var("SELECT opt_value FROM ".$wpdb->prefix."custom_options WHERE id = " . $_GET['id']);

	    $del = $wpdb->query("DELETE FROM ".$wpdb->prefix."custom_options WHERE id=" . $_GET['id']);

	    if($filename != '') {
		echo '<div class="updated">';
		if(unlink(UPLOADSPATH.'/ban/'.$filename)) {
		    echo '<p>Success! The file has been deleted.</p>';
		} else {
		    echo '<p>There was a problem removing that file.</p>';
		}
    
		if($del) {
		    echo '<p>Success! The record has been deleted.</p>';
		} else {
		    echo '<p>There was a problem removing that record.</p>';
		}
	    echo '</div>';
	    }

    }

    if(isset($_POST['addBanner'])){
	    echo '<div id="message" class="updated fade">';
	    if(is_uploaded_file($_FILES['opt_value']['tmp_name'])) {

		if(!is_dir(UPLOADSPATH.'/ban')) {
		    mkdir(UPLOADSPATH.'/ban');
		}

		$filetype = $_FILES['opt_value']['type'];
		if ($filetype != 'image/jpeg' && $filetype != 'image/gif' && $filetype != 'image/png') {
		    echo '<span class="error">The file needs to be either jpg, gif, or png.  Please try again.</span>';
		} else { //IS CORRECT TYPE;

		    $imagename = str_replace(' ','_',$_FILES['opt_value']['name']);

		    $uploaddir = UPLOADSPATH.'/ban/';

		    $filefullpath = $uploaddir . $imagename;

		    if (move_uploaded_file($_FILES['opt_value']['tmp_name'], $filefullpath)) {

			list($w,$h) = getimagesize($filefullpath);
			
			if($_POST['opt_option'] != '#'){
				if($w > 225 || $h > 285) {
					resizeImage($filefullpath,$filetype,225,285);
				}
			}

		    } else
			    echo '<span class="error">Error uploading file.  Code 755</span>';

		    $opt_option = $_POST['opt_option'];

		    if($wpdb->query("INSERT INTO ".$wpdb->prefix."custom_options (opt_name,opt_value,opt_option) VALUES ('homeBanner','$imagename','$opt_option')"))
			    echo 'Added new image to the slideshow.';
		    else
			    echo '<span class="error">There was an error adding the image to the slideshow.</span>.';
		}
	    } else
		    echo '<span class="error">You didn\'t choose a file?</span>';

	    echo '</div>';

    } else if(isset($_POST['editRec'])) {

	    echo '<div class="updated">';

	    $id = $_POST['editRec'];
	    $opt_option = $_POST['opt_option'];

	    if($wpdb->query("UPDATE ".$wpdb->prefix."custom_options SET opt_option='$opt_option' WHERE id=$id"))
		    echo 'Updated the information for ' . $_POST['opt_value'];
	    else
		    echo '<span class="error">There was an error updating that information.</span>.';

	    echo '</div>';
    }

    /************  END ACTION SECTION  **************/
    ?>

	    <form class="lookForPrefill" style="border:1px solid gray;padding:20px;" action="" method="POST" enctype="multipart/form-data">
	      <h3>Add a Banner</h3>
		    Images should be 225 pixels wide &amp; 285 pixels tall and have an extension of <em>GIF, JPEG, JPG, or PNG</em>.
		    <table><tr>
			    <td>Image file </td>
			    <td><input type="file" name="opt_value" id="file"/></td>
		      </tr><tr>
			    <td>Links to </td>
			    <td><input data-prefill="#" type="text" name="opt_option" value="" /> <i>Use <b>#</b> for none.</td>
		      </tr><tr>
			<td><input type="hidden" name="addBanner" value="true">
			    <input type="submit" value="Add to Banners" /></td>
			    <td>&nbsp;</td>
		    </tr></table>

	    </form>


	    <?php
	    $slides = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."custom_options WHERE opt_name='homeBanner'");
	    if($slides) :

		echo '<div style="border: 1px solid gray;padding:20px;overflow:auto;"><h3>Manage Existing Banners</h3>';
		foreach($slides as $sl) {

		    echo '<form style="border-bottom:1px dotted silver;padding: 20px;overflow:auto;float:left;" method="post" action="?page=' . $_GET['page'] . '">
			<input type="hidden" name="editRec" value="'.$sl->id.'" />
			<input type="hidden" name="opt_value" value="'.$sl->opt_value.'" />

			<img style="border:1px solid #000;" src="'.UPLOADSDIR.'/ban/'.$sl->opt_value.'" alt="" /><br>

			Links to: <input style="padding:5px; width:400px;" type="text" name="opt_option" value="'.$sl->opt_option.'" /><br>

			<input type="submit" value="Save Changes" />
			<a class="delete" style="color:red;" title="'.$sl->opt_value.'" data-id="'.$sl->id.'" href="?page='.$_GET['page'].'" >delete this</a></td>

		    </form>
		    ';

		}
		echo '</div>';
	    endif;

    echo '</div>'; // WRAP;
    }

// end homepage banners


// AJAXING CODE
add_filter('query_vars','plugin_add_trigger');
function plugin_add_trigger($vars) {
    $vars[] = 'lplAjax';
    return $vars;
}
add_action('template_redirect', 'plugin_trigger_check');
function plugin_trigger_check() {
    global $wpdb;
    if(get_query_var('lplAjax') == 1) {

	/* AJAXING HERE */

      if(isset($_GET['getEvents'])) {

	if($_GET['category'] != 'all') {
	    $cat = htmlspecialchars($_GET['category']);
	    $events = mejp_getEvents(3,'future',$cat);
	} else
	    $events = mejp_getEvents(3);

	// error_log( var_export($events,1));
	if ($events) :

	    $shown = array();

	    foreach($events as $eventDate => $evsForDate) {
			foreach($evsForDate as $evOccurances) {
				foreach($evOccurances as $ev) {
					if(count($shown) >= 3) {
						break;
					}
					if($ev->eGroup == 1) {
						// don't show the event multiple times...
						if(in_array($ev->ID,$shown)) continue;
					}

					?>
					<div class="eventWrap style="display:none;">
						<a href="<?php echo $ev->guid; ?>"><?php echo jp_get_featured_image($ev->ID) ?></a>
						<h2><a href="<?php echo $ev->guid; ?>"><?php echo $ev->post_title; ?></a></h2>
						<p class="eventDate"><?php echo date('M d, Y', strtotime($eventDate)) . ' &mdash; ' . $ev->time .'</p>';

						$showChars = 150;

						$desc = strip_tags($ev->post_content);
						list($desc) = str_split($desc,$showChars);
						echo '<p>' . $desc;
						if(strlen( $ev->post_content ) > $showChars)
						echo '...<a href="' . $ev->guid . '">read more</a>.';

						echo '</p>';
						?>
					</div>
					<?php
					$shown[] = $ev->ID;
				}
			}
	    }


	    else : ?>

	    <div class="singleEvent">
		<p>No events at the moment.</p>
	    </div>

	    <?php endif;
      }


    /* END AJAX */

    exit;
    }
}


add_action('phpmailer_init',function($phpmailer){
	
    // Define that we are sending with SMTP
    // $phpmailer->isSMTP();
 
    // The hostname of the mail server
    // $phpmailer->Host = 'gt500.websitewelcome.com';
    // $phpmailer->Host = 'mail.lplonline.org';
 
    // Use SMTP authentication (true|false)
    $phpmailer->SMTPAuth = false;
 
    // SMTP port number - likely to be 25, 465 or 587
    // $phpmailer->Port = 25;
 
    // // Username to use for SMTP authentication
    // $phpmailer->Username = 'noreply@lplonline.org';
 
    // // Password to use for SMTP authentication
    // $phpmailer->Password ='28c2nzvk';
 
    // // Encryption system to use - ssl or tls
    // // $phpmailer->SMTPSecure = 'ssl';
 
    $phpmailer->From = 'noreply@lplonline.org';
    $phpmailer->FromName = 'LPLonline emailer';
});


add_action('wp_mail',function($args){
	if( is_array($args['headers']) ) 
		array_push($args['headers'] , 'Content-Type: text/html');
	else
		$args['headers'] = array('Content-Type: text/html');
	
	return $args;
});


add_action('admin_menu',function(){
	add_submenu_page( 'edit.php?post_type=page', 'Homepage Important Message', 'Homepage Important Message', 'edit_posts', 'homepage-important-message', 'lplonline_homepageImportantMessage_adminPage');
});

function lplonline_homepageImportantMessage_adminPage(){
	require 'admin-page-importantMessage.php';
}

/**
 * Add setting to control whether homepage displays all events or adult events
 * initially
 */
add_action('admin_init', function(){

	register_setting( 'general', 'lpl_homepage_events_initial_display');
	add_settings_section( 'homepage-events', 'Display of Events on Homepage', 'lplonline_homepageEvents_InitialCategory_section', 'general' );
	add_settings_field( 'lpl_homepage_events_initial_display', 'Initial category to display', 'lplonline_homepageEvents_InitialCategory_field', 'general','homepage-events');

	function lplonline_homepageEvents_InitialCategory_section(){
		echo '<p>Settings related to how events are handled on the homepage.';
	}
	function lplonline_homepageEvents_InitialCategory_field(){
		$events_initial_display = get_option('lpl_homepage_events_initial_display', 'all');
		echo '<select name="lpl_homepage_events_initial_display" id="lpl_homepage_events_initial_display">
			<option value="all" '.selected( 'all', $events_initial_display, 0 ).'>All</option>
			<option value="adults" '.selected( 'adults', $events_initial_display, 0 ).'>Adults</option>
			<option value="teens" '.selected( 'teens', $events_initial_display, 0 ).'>Teens</option>
			<option value="kids" '.selected( 'kids', $events_initial_display, 0 ).'>Kids</option>
		</select>';
	}
});


add_action('after_setup_theme', function(){
	register_nav_menu( 'main-navigation', 'Main Navigation' );
});


/** Custom Updates
 *
 * Will fetch updates based on tags in repo
 */
require 'updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/lplonline.org-theme/',
	__FILE__, //Full path to the main plugin file or functions.php.
	'cleanGrayLPL'
);

