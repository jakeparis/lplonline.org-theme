<?php

/* Wordpress not loaded */

$domain = 'lplonline.org';
$to = 'lplreference@gmail.com';

if(isset($_POST['salutation'])) {

    if($_POST['salutation'] == '') {

    $headers = array(
    	'Content-Type: text/html'
    );

	$msg = '<div id="message">';

	$hQuestion = (isset($_POST['hQuestion'])) ? strip_tags( htmlentities($_POST['hQuestion']) ) : '';
	$hName = (isset($_POST['hName'])) ? strip_tags( htmlentities($_POST['hName']) ) : '';
	$hContact = (isset($_POST['hContact'])) ? strip_tags( htmlentities($_POST['hContact']) ) : '';

	setcookie('patronName',$hName, time()+60*60*24*30*6, '/'); // 6 months
	setcookie('patronContact',$hContact, time()+60*60*24*30*6, '/'); // 6 months

	$lists = array();
	if( isset($_POST['mailingListKids']) ) $lists[] = 'mailingListKids';
	if( isset($_POST['mailingListAdults']) ) $lists[] = 'mailingListAdults';
	if( isset($_POST['mailingListTeens']) ) $lists[] = 'mailingListTeens';


	// if they gave us an email, run it through
	if(strpos($hContact,'@') !== FALSE && strpos($hContact,'.') !== FALSE ) {

	    $email = $hContact;
	    $headers[] = "Reply-To: {$email}";

	    if(!empty($lists)) {

		if(strpos($name,' ') !== FALSE) {

		    list($fname,$lname) = explode(' ',$regName);
		    if($fname != '')
			$merge_vars['FNAME'] = $fname;
		    if($lname != '')
			$merge_vars['LNAME'] = $lname;

		} else {
		    $merge_vars['FNAME'] = $name;
		}

		$lplLists = '';
		if( in_array( 'mailingListTeens', $lists) ) {
		    $lplLists = 'Teen Events,';

		}
		if( in_array( 'mailingListKids', $lists) ) {
		    $lplLists .= 'Children\'s Events,';

		}
		if( in_array( 'mailingListAdults', $lists) ) {
		    $lplLists .= 'Adult Events';

		}
		trim($lplLists,',');

		if($lplLists != '') {

		    $merge_vars['GROUPINGS'] = array(
			    array('name'=>'Send me...', 'groups'=> $lplLists)
		    );
		    $merge_vars['MC_NOTES'] = array(
			'note' => ' Signed up from '.$domain.' contact form.'
		    );

		    $apikey = '024ebda876429ae0b47e6f81a0594eac-us6';
		    require_once('lib/MCAPI.class.php');
		    $api = new MCAPI($apikey);
		    $listId = 'dbbfa932a9';

		    $retval = $api->listSubscribe( $listId, $email, $merge_vars, 'html', false, true );

		    if (!$api->errorCode) {
			$signupResults = 'You have been signed up for the ' . $lplLists . ' Mailings. ';
		    } else
			$signupResults = 'Error signing up';

		}

	    }

	}

	/* // take out verification, since JS is doing that
	if( $hQuestion == '' || $hName == '' ) {
		$msg .= 'You missed a required field.';

	} else {
	    // all checks out, do stuff
	*/
	    date_default_timezone_set('America/New_York');


	    $body = '
		    <html>
			<head>
			    <style type="text/css">
			    td { border-bottom: 1px solid #cfcfcf; padding:4px 8px;}
			    ul { margin-left: 0; padding-left: 0;}
			    li { margin-left: 0; padding-left: 0;}
			    </style>
			</head>
			<body>

			<b>' . date('l, M j \a\t g:ia') . '</b>

			<table>
			    <tr>
				<td>Name</td><td><b>' . $hName . '</b></td>
			    </tr>
			    <tr>
				<td>Contact Info</td><td><b>' . $hContact . '</b></td>
			    </tr>
			    <tr>
				 <td>Message</td><td><b>' . $hQuestion . '</b></td>
			    </tr>';

		if(!empty($lists)) {

		    $body .= '<tr>
			<td>Signed up for</td><td><ul>';

		    foreach ( $lists as $list) {
			$body .= '<li>' . $list . '</li>';
		    }

		    $body .= '</ul>
			</td></tr>';
		}


		$body .= '
			</table>

			</body></html>';


	    $subject = "Contact Form submission: ($hName)";

	    if($headers)
	    	$headers = implode("\r\n", $headers);

		$result = mail($to,$subject,$body,$headers);

	    if($result) {
		    $msg .= '<p class="success"><b>Your message was sent</b>.<br> ' . $signupResults . ' Thank you!</p>';
		    unset($regName,$phone,$email,$message);
	    } else {
		    $msg .= '<p class="error">Error! There was an unknown error sending your message. Please try again later.</p>';
	    }
	//} // verification
	$msg .= '</div>';

	echo $msg;
    } else echo 'salutation no empty';
} else echo 'salutation not set';
