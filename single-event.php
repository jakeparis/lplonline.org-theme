<?php get_header(); ?>

<div id="middle">

    <div id="main" role="main" page="single">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>



	<h1><?php the_title(); ?></h1>

	<?php

    echo '<div id="contentWrap">';

	the_content(); ?>

    </div>

    <div class="meta">

		<?php
		$sidebarToShow = null;

		$evcats = get_the_terms($post->ID,'event-tag');
		if( $evcats != false && ! is_wp_error($evcats) ) {

			echo '<p>See more in: ';
			foreach($evcats as $evc) {
				$cArr[] =  '<a href="'.get_term_link( $evc->slug,'event-tag' ).'">'.$evc->name.'</a>';

				// I know the latter one will override the former if both terms are present.
				// limitation of crappy architecture.
				if( $evc->slug === 'kids' )
					$sidebarToShow = 'kids';
				if( $evc->slug === 'teens' )
					$sidebarToShow = 'teens';
				
			}
			$cStr = implode(' | ',$cArr);
			echo $cStr . '</p>';

		}
		?>
		
		<!-- <p>Written on: <?php the_date('M d, Y'); ?></p> -->

    </div>
    <?php endwhile; endif; ?>



    </div>


    <?php get_sidebar($sidebarToShow); ?>


</div>

<?php get_footer(); ?>