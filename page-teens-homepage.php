<?php
/*
Template Name: Teens-- Homepage
*/
get_header('teens'); ?>

<div id="middle">

    <div id="main" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php the_content(); //basic page content

    endwhile; endif; // end basic page loop

    //begin posts loop
    $teenPosts = new WP_Query('category_name=teen-post&posts_per_page=5');

    if ($teenPosts->have_posts()) : while ($teenPosts->have_posts()) : $teenPosts->the_post(); ?>

	<h2><?php the_title(); ?></h2>

	<?php the_content(); ?>

	<div class="meta">

	    <p>Written on <?php echo get_the_date('M d, Y'); ?> by <?php the_author(); ?>
	    <?php the_tags(' and tagged with <b>','</b> &bull; <b>','</b>'); ?>
	    </p>

	</div>

    <?php endwhile; endif; // end teen posts loop ?>







    </div><!--#main-->


    <?php get_sidebar('teens'); ?>


</div>

<?php get_footer('teens'); ?>