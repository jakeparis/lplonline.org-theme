<div role="complementary" id="sidebar">
<?php
$events = mejp_getEvents(3, 'future', 'kids');
if( isset($_GET['jake']) ) {
    $events = Jp_Multi_Events::instance()->get_events([
        'futureOnly' => true,
        'limit' => 3,
        'onlyTags' => 'kids'
    ]);
    echo '<pre>---$events---<br>';var_dump($events);echo'</pre>';
}
if ($events) :

echo '<h2>Upcoming Kids Events</h2>';
$odd = '';
$shown = array();

foreach($events as $eventDate => $evsForDate) {
    foreach($evsForDate as $evOccurances) {
        foreach($evOccurances as $ev) {

            if( count($shown) > 2 )
                break 3;
                
            if($ev->eGroup == 1) {
            // don't show the event multiple times...
            if(in_array($ev->ID,$shown)) continue;
            }
            ?>

            <div class="singleEvent" style="padding-left:5px;">
            <h2><a href="<?php echo $ev->guid; ?>"><?php echo jp_get_featured_image($ev->ID) . $ev->post_title; ?></a></h2>
            <p class="eventDate"><?php echo date('M d, Y', strtotime($eventDate)) . ' &mdash; ' . $ev->time .'</p>';

            $showChars = 150;

            $desc = strip_tags($ev->post_content);
            list($desc) = str_split($desc,$showChars);
            echo '<p>' . $desc;
            if(strlen( $ev->post_content ) > $showChars)
                echo '...<a href="' . $ev->guid . '">read more</a>.';

            echo '</p>';
            ?>
            </div>


            <?php
            $odd = ($odd == 'odd') ? '' : 'odd';

            $shown[] = $ev->ID;
        }
    }
}

echo '<p style="text-align:center;background:transparent"><b>See <a href="' . get_permalink(977) . '">all upcoming events for kids</a>.</b></p>';

else : ?>

<p>No events at the moment.</p>

<form class="helpForm" id="helpFormSidebar" method="post" action="">
    <img class="ajaxLoader" src="<?PHP echo THEMEDIR; ?>/i/ajax-loader.gif">
    <input type="text" name="salutation" id="salutation" value="">
    <h5>Need Help? Ask Here.</h5>
    <p style="color: #333;font-size:.8em;">We try to respond to everyone within a day. Want a quicker answer? <a href="#" class="openChatWindow" id="chatSplash">Chat now with a librarian now</a>!</p>
    <label>Name</label>
    <input type="text" name="helpFormName" value="" class="ajaxFill">
    <label>Contact info (email or phone)</label>
    <input type="text" name="helpFormContact" value="" class="ajaxFill">
    <label>Question / Comment</label>
    <textarea name="helpFormQuestion"></textarea>
    <div id="helpFormMailingLists" style="color: #777;">
	<p><b>Signup for our Mailing Lists?</b></p>
	<label for="mailingListAdults"><input type="checkbox" name="mailingListAdults" value="1" checked="checked" /> Adult Events</label><br>
	<label for="mailingListTeens"><input type="checkbox" name="mailingListTeens" value="1" checked="checked" /> Teen Events</label><br>
	<label for="mailingListKids"><input type="checkbox" name="mailingListKids" value="1" checked="checked" /> Kids Events</label><br>
    </div>
    <input type="submit" value="Send Your Question" name="helpFormSubmit">
	</form>
	



<?php endif; ?>
</div>
