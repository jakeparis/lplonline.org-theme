<?php
/*
Template Name: Bookletters Template
*/
get_header(); ?>

<div id="middle">

    <div id="main" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post();


	if($post->post_parent != 0) { 

		define('SHOW_BOOKLETTERS_LIST', '1');

	}
	?>


	<h1><?php the_title(); ?></h1>


	<?php

	/* use a custom meta field called 'bookletters-code'
	* and enter: newsletter-code comma audience-number
	* like: cnl12,au13
	*/

	$code = get_post_meta($post->ID,'bookletters-code',true);

	if(strpos($code,',') !== FALSE) {
	    // the code was entered as newsletter-code comma audience-number
	    $codes = explode(',',$code);
	} else {
	    $codes = $code; // string with just newsletter code
	}


	if(is_array($codes)) {
	    // if an audience code was in the custom meta field, print a signup form
	    echo '
	    <iframe class="bookLetterIframe" title="books" src="//library.booksite.com/6229/nl/?list=' . $codes[0]. '" frameborder="0"
	    	></iframe>

	    <div style="bookLettersSignupForm">
	    <p>Signup to get this list delivered to your inbox each month.</p>

	    <form method="post" action="http://library.booksite.com/6229/signupSubscribe/?list=NLSGN" class="lookForPrefill">
		<input type="hidden" name="list[]" value="' . $codes[1] . '" />	<input type="hidden" name="sid" value="6229" />
		<input data-prefill="email" aria-label="Email" type="text" id="email" name="email" /> <input type="submit" value="Subscribe" />
	    </form>
	    </div>
	    ';
	} else if($codes == 'NLSGN') { // if we are on the full signup list page, it needs a diff url

	    echo '<iframe src="http://library.booksite.com/6229/signup/?list=NLSGN" frameborder="0" class="bookLetterIframe"></iframe>';

	} else { // a newsletter page

	    echo '<iframe src="http://library.booksite.com/6229/nl/?list=' . $codes. '" frameborder="0" class="bookLetterIframe"></iframe>';

	}
?>
<div class="meta">

	<p class="breadcrumbs"><b>You are Here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>
<?php
    endwhile; endif; ?>



    </div>


    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>