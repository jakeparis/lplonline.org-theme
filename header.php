
<?php

get_template_part('snippet', 'header');
?>

<span id="navWrap" role="navigation">
<?php
wp_nav_menu(array(
	'theme_location' => 'main-navigation',
	'menu'=>'main-nav',
	'container'=>'',
	'menu_id'=>'mainNav'
));

?>

</span>