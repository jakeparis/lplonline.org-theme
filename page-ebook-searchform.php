<?php 
/* 
Template Name: Ebooks Search Form
*/
get_header(); ?>

<div id="middle">

    <div id="main" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>

    <form action="//ebook.yourcloudlibrary.com/library/lewistonpl/Search/" method="post" id="inline-ebook-search">
        <input aria-label="Search" name="s" type="text" /> 
        <input type="submit" value="search eBooks" />
    </form>

    <script>
    jQuery(function($){
        var $ebs = $("#inline-ebook-search");
        $ebs.on('submit',function(e){
            var val = $(this).find('input[name="s"]').val();
            var action = $(this).attr('action');
            val = val.replace(/[&"><\']/g,'');
            $(this).attr('action', action + val);
        });
    });
    </script>

    <div class="meta">

	<p class="breadcrumbs"><b>You are Here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>
    <?php endwhile; endif; ?>



    </div>


    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>
