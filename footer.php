﻿
<footer>

	
    <?php if(!isset($_GET['test'])) { ?>

      <!--
      <div id="gr_grid_widget_1440535152" class="good-reads-widget" style="grid-column: 1 / 3">
        <!-- Show static html as a placeholder in case js is not enabled - javascript include will override this if things work
            <h2>
              <a href="https://www.goodreads.com/review/list/30926536-lewiston-public-library?shelf=read&utm_medium=api&utm_source=grid_widget" style="text-decoration: none;">Lewiston's bookshelf: read</a>
            </h2>
          <div class="gr_grid_container">
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/550199.We_Have_Always_Lived_in_the_Castle" title="We Have Always Lived in the Castle"><img alt="We Have Always Lived in the Castle" border="0" src="https://d.gr-assets.com/books/1347471772s/550199.jpg" /></a></div>
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/18295821-the-adventures-of-beekle" title="The Adventures of Beekle: The Unimaginary Friend"><img alt="The Adventures of Beekle: The Unimaginary Friend" border="0" src="https://d.gr-assets.com/books/1424308067s/18295821.jpg" /></a></div>
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/17242447-ketchup-clouds" title="Ketchup Clouds"><img alt="Ketchup Clouds" border="0" src="https://d.gr-assets.com/books/1360574323s/17242447.jpg" /></a></div>
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/933660.501_Spanish_Verbs" title="501 Spanish Verbs (5th Edition)"><img alt="501 Spanish Verbs" border="0" src="https://d.gr-assets.com/books/1387739436s/933660.jpg" /></a></div>
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/18166920-beyond-magenta" title="Beyond Magenta: Transgender Teens Speak Out"><img alt="Beyond Magenta: Transgender Teens Speak Out" border="0" src="https://d.gr-assets.com/books/1384017436s/18166920.jpg" /></a></div>
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/1516179.Minerva_Louise_at_the_Fair" title="Minerva Louise  at  the  Fair"><img alt="Minerva Louise  at  the  Fair" border="0" src="https://d.gr-assets.com/books/1184564551s/1516179.jpg" /></a></div>
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/4021507-three-stories-you-can-read-to-your-dog" title="Three Stories You Can Read to Your Dog"><img alt="Three Stories You Can Read to Your Dog" border="0" src="https://d.gr-assets.com/books/1267149677s/4021507.jpg" /></a></div>
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/9408479-cold-wind" title="Cold Wind (Joe Pickett, #11)"><img alt="Cold Wind" border="0" src="https://d.gr-assets.com/books/1440027962s/9408479.jpg" /></a></div>
            <div class="gr_grid_book_container"><a href="https://www.goodreads.com/book/show/1516175.Minerva_Louise_at_School" title="Minerva Louise at School"><img alt="Minerva Louise at School" border="0" src="https://d.gr-assets.com/books/1184564549s/1516175.jpg" /></a></div>
          
        <noscript><br/>Share <a href="/">book reviews</a> and ratings with Lewiston, and even join a <a href="/group">book club</a> on Goodreads.</noscript> 
      </div>

      </div>
      -->
      <!-- <script src="https://www.goodreads.com/review/grid_widget/30926536.Lewiston's%20bookshelf:%20read?cover_size=small&hide_link=&hide_title=&num_books=9&order=d&shelf=read&sort=date_added&widget_id=1440535152" type="text/javascript" charset="utf-8"></script> -->


    <div id="contactFoot">
      <p><b>Lewiston Public Library</b> is a department of the <a href="http://www.lewistonmaine.gov">City of Lewiston</a></p>

      <p>
        200 Lisbon Street, Lewiston, ME 04240<br>
        <a href="mailto:lplreference@lewistonmaine.gov">LPLReference@LewistonMaine.gov</a><br>
        207-513-3004
      </p>

      <p>
      Monday-Thursday: 9:30AM - 7PM<br>
      <?php if(!summerHours()) { ?>
		    Friday-Saturday : 9:30AM - 1PM
		<?php } else { ?>
		    Friday: 9:30AM - 1PM <br>
		    Saturday: 9:30AM - 1PM
		<?php } ?><br>
		Sunday: Closed
      </p>

      <?php wp_nav_menu(array('menu'=>'footer-menu','container'=>'', 'menu_class'=> 'no-bullet'));  ?>

      <p><b>Librarians</b></p>

      <ul class="no-bullet">
          <li>Library Director &mdash; <a href="mailto:jhouston@lewistonmaine.gov">Joseph Houston</a></li>
          <li>Adult Services Librarian &mdash; <a href="mailto:sgbouchard@lewistonmaine.gov">Steven Bouchard</a></li>
          <li>Children's Librarian &mdash; <a href="mailto:sturner@lewistonmaine.gov">Sara Turner</a></li>
          <li>Collection Services Librarian &mdash; <a href="mailto:ealmquist@lewistonmaine.gov">Elizabeth Almquist</a></li>
          <li>Lending Services Librarian &mdash; <a href="mailto:kwebber@lewistonmaine.gov">Katherine Webber</a></li>
      </ul>

    </div>

    <div id="socialIcons">
		<a target="_blank" class="social-icon" href="https://www.facebook.com/lewistonlibrary" title="Facebook"></a>
		<a target="_blank" class="social-icon" href="https://www.twitter.com/lplonline" title="Twitter"></a>
		<a target="_blank" class="social-icon" href="https://www.pinterest.com/lplonline/" title="Pinterest"></a>
		<a target="_blank" class="social-icon" href="https://www.goodreads.com/user/show/30926536-lewiston-public-library" title="GoodReads"></a>
		<a target="_blank" class="social-icon" href="https://www.youtube.com/channel/UCGMPfxfJCyBJEmxkR-7tgjg/" title="YouTube"></a>
		<a target="_blank" class="social-icon" href="https://www.instagram.com/lplonline" title="Instagram"></a>
		
    </div>

    <p id="copyright">site &copy; <?php echo date('Y'); ?> Lewiston Public Library<br>
    web hosting donated by <a target="_blank" href="http://thcreations.com/">TH Creations</a></p>

</footer>

</div><!--container-->
<?php wp_footer(); ?>
<?php } ?> 
</body>
</html>