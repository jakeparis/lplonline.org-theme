<?php
/*
Template Name: Library Card Regis.
*/

add_action('wp_enqueue_scripts', function(){
	echo '<script src="https://www.google.com/recaptcha/api.js"></script>' . "\n";
});


if(isset($_POST['salutation']) && $_POST['salutation'] == '') {

	$recaptchaUrl = 'https://www.google.com/recaptcha/api/siteverify';
	$recaptchaKey = '6LftFtoZAAAAAAo2m6pUS7nm5uL8Yk1R5D8ijJJx';
	$ch = curl_init($recaptchaUrl);
	curl_setopt_array($ch, array(
	    CURLOPT_RETURNTRANSFER => true
	    ,CURLOPT_TIMEOUT => 4 // seconds
	    ,CURLOPT_POST => true
	    ,CURLOPT_POSTFIELDS => array(
	    	'secret' => $recaptchaKey,
	    	'response' => $_POST['g-recaptcha-response'],
	    ),
	));
	$json = curl_exec($ch);
	curl_close($ch);
	$recaptchaVerify = json_decode($json);

    $actionresponse = '<div id="message">';

    $regName = strip_tags($_POST['regName']);
    $regType = $_POST['regType'];

    $address = strip_tags($_POST['regAddress']);
    $address2 = strip_tags($_POST['regAddress2']);
    $phone = strip_tags($_POST['regPhone']);

    $dob = str_replace('/','-',strip_tags($_POST['regDob']));

    $email = strip_tags($_POST['regEmail']);
    $cardno = strip_tags($_POST['cardno']);

    $parent = strip_tags($_POST['regParent']);
    $contactName = strip_tags($_POST['regContactName']);
    $contactPhone = strip_tags($_POST['regContactPhone']);

    if(is_uploaded_file($_FILES['regIdUpload']['tmp_name'])) {
	$filename = $_FILES['regIdUpload']['name'];
	$filetype = $_FILES['regIdUpload']['type'];
    } else
	$filename = '';

    if(isset($_POST['regMailingListSignups'])) {
		foreach($_POST['regMailingListSignups'] as $ls) {
			$mailinglist[] = $ls;
		}
    }

	if(
	$regName == '' ||
	$address == '' ||
	$phone == '' ||
	$dob == '' ||
	$regType == '' ||
	$email == ''
	) {
		$actionresponse .= '<p class="error">You missed a required field. Please fill in <i>all</i> fields marked with <span class="required"></span></p><script>
		jQuery(function($){
		    $("#firstThreeRegQuestions").hide();
		    $("#secondRegQuestions").show();';

		    if($parent != '')
			$actionresponse .= '$("#regContactWrap").hide();';
		    else
			$actionresponse .= '$("#regParentWrap").hide();';

		    if($filename == '')
			$actionresponse .= '$("#regIdUploadWrap").hide();';

		    if($cardno == '')
			$actionresponse .= '$("#regCardNumberWrap").hide();';

		$actionresponse .= '
		});
		</script>';

	} elseif( $recaptchaVerify->success == false ) {
		
		$actionresponse .= '<p class="error">The "I am not a robot" box was not checked. Please try again.</p>
		<script>
		jQuery(function($){
		    $("#firstThreeRegQuestions").hide();
		    $("#secondRegQuestions").show();';

		    if($parent != '')
				$actionresponse .= '$("#regContactWrap").hide();';
		    else
				$actionresponse .= '$("#regParentWrap").hide();';

		    if($filename == '')
				$actionresponse .= '$("#regIdUploadWrap").hide();';

		    if($cardno == '')
				$actionresponse .= '$("#regCardNumberWrap").hide();';

		$actionresponse .= '
		});
		</script>';

	} else if( ($contactName == '' || $contactPhone == '') && $parent == '') {
	    if($_POST['regAge'] == 'ageKid') {

		$actionresponse .= '<p class="error">Please fill in the Parent\'s Name.</p>
		<script>
		jQuery(function($){
		    $("#firstThreeRegQuestions").hide();
		    $("#secondRegQuestions").show();
		    $("#regContactWrap").hide();';

		if($filename == '') $actionresponse .= '$("#regIdUploadWrap").hide();';

		if($cardno == '') $actionresponse .= '$("#regCardNumberWrap").hide();';

		$actionresponse .= '
		});
		</script>';

	    } else {
		$actionresponse .= '<p class="error">Please fill in the name and phone number of a person who doesn\'t live with you.</p>
		<script>
		jQuery(function($){
		    $("#firstThreeRegQuestions").hide();
		    $("#secondRegQuestions").show();
		    $("#regParentWrap").hide();';

		    if($filename == '') $actionresponse .= '$("#regIdUploadWrap").hide();';

		    if($cardno == '') $actionresponse .= '$("#regCardNumberWrap").hide();';

		$actionresponse .= '});
		</script>';
	    }
	} else if(strpos($email,'@') == FALSE || strpos($email,'.') == FALSE) {
	    $actionresponse .= '<p class="error">Email address was not in the correct format.</p>
		<script>
		jQuery(function($){
		    $("#firstThreeRegQuestions").hide();
		    $("#secondRegQuestions").show();
		    $("#regContactWrap").hide();
		    $("#regParentWrap").hide()';

		    if($filename == '') $actionresponse .= '$("#regIdUploadWrap").hide();';

		    if($cardno == '') $actionresponse .= '$("#regCardNumberWrap").hide();';

		$actionresponse .= '
		});
		</script>';

	} else if ($filename != '' && $filetype != 'image/jpeg' && $filetype != 'image/png' && $filetype != 'application/pdf') {
	    // if file upload, validate filetype (jpg or pdf)
	    $actionresponse .= '<p class="error">Your scanned ID must be in either <b>jpg</b>, <b>png</b>, or <b>pdf</b> format.</p>
		<script>
		jQuery(function($){
		    $("#firstThreeRegQuestions").hide();
		    $("#secondRegQuestions").show();';

		    if($parent != '') $actionresponse .= '$("#regContactWrap").hide();';
		    else $actionresponse .= '$("#regParentWrap").hide();';

		    $actionresponse .= '
		    $("#regCardNumberWrap").hide();
		});
		</script>';

	} else if($filename != '' && $_FILES['regIdUpload']['size'] > 2000000) {// check filesize, 2mb max
	    $actionresponse .= '<p class="error">Your file was too large. Please try again with a file less than 1 Megabyte.</p>
		<script>
		jQuery(function($){
		    $("#firstThreeRegQuestions").hide();
		    $("#secondRegQuestions").show();';

		    if($parent != '') $actionresponse .= '$("#regContactWrap").hide();';
		    else $actionresponse .= '$("#regParentWrap").hide();';

		    $actionresponse .= '
		    $("#regCardNumberWrap").hide();
		});
		</script>';

	} else {


		setcookie('patronName',$regName, time()+60*60*24*30*6, '/'); // 6 months
		setcookie('patronContact',$email, time()+60*60*24*30*6, '/'); // 6 months
		setcookie('patronCardno',$cardno, time()+60*60*24*30*6, '/'); // 6 months

		// is this person under 12 or under?
		//$dobForm = date('Y-m-d',strtotime($dob));
		//$birthDate = explode('-',$dobForm);
		//$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md") ? ((date("Y")-$birthDate[0])-1):(date("Y")-$birthDate[0]));

		if($_POST['regAge'] == 'ageKid') {
		    $to = 'lplKids@lewistonmaine.gov';
		} else {
		    $to = 'lplcirc@gmail.com';
		}

		/* TESTING
		/**/
		// $to = 'web@jake.paris';
		/**/

		if( isset($email) && ! empty($email) )
			$headers[] = "Reply-To: $email";

		$subject = "Online Registration or Renewal ($regName)";

		
		// handle mailing list signups

		if(isset($mailinglist)) {

		    // AUBURN LIST (CONSTANT CONTACT)
		    if(in_array('regMailingAuburn',$mailinglist)) {
			//signup for auburn mailings

			$signedUpFor[] = 'APL mailing list';
			$mailingListMessage .= 'You have been signed up for the Auburn Public Library\'s Monthly Events Mailing. ';

		    }

		    // LEWISTON LIST (MAILCHIMP)
		    if(
			    in_array( 'regMailingLewistonTeens', $mailinglist) ||
			    in_array( 'regMailingLewistonAdults', $mailinglist) ||
			    in_array( 'regMailingLewistonKids', $mailinglist)
		    ) {

			//signup for lewiston mailings (mailchimp)

			list($fname,$lname) = explode(' ',$regName);
			if($fname != '')
			    $merge_vars['FNAME'] = $fname;
			if($lname != '')
			    $merge_vars['LNAME'] = $lname;

			if(	in_array( 'regMailingLewistonTeens', $mailinglist) ) {
			    $lplLists = 'Teen Events,';
			    $signedUpFor[] = 'LPL Teen events list';
			}
			if(	in_array( 'regMailingLewistonKids', $mailinglist) ) {
			    $lplLists .= 'Children\'s Events,';
			    $signedUpFor[] = 'LPL Kids events list';
			}
			if(	in_array( 'regMailingLewistonAdults', $mailinglist) ) {
			    $lplLists .= 'Adult Events';
			    $signedUpFor[] = 'LPL Adult events list';
			}
			trim($lplLists,',');

			$merge_vars['GROUPINGS'] = array(
				array('name'=>'Send me...', 'groups'=> $lplLists)
			);
			$merge_vars['MC_NOTES'] = array(
			    'note' => ' Signed up from libraryla.org registration form.'
			);

			$apikey = '024ebda876429ae0b47e6f81a0594eac-us6';
			require_once 'lib/MCAPI.class.php';
			$api = new MCAPI($apikey);
			$listId = 'dbbfa932a9';

			$retval = $api->listSubscribe( $listId, $email, $merge_vars, 'html', false, true );

			if (!$api->errorCode) {
			    $mailingListMessage .= 'You have been signed up for the Lewiston Public Library\'s Event Mailings (' . $lplLists . '). ';
				}
		    } // end mailing list signups
		}

		// all checks out, do stuff

		$body .= '
			<html>
				<head>
					<style type="text/css">
					td {border:1px solid #ccc;padding: 1px;}
					</style>
				</head>
				<body>
				<p><span style="color:#AC3201;"><b>Please remember to check for an existing card before making a new one.</b></span><br />
				Replies to this message will be sent to the patrons\'s email address ( '.$email.' )</p>
				<hr />
				<table>
					<tr>
						<td>Type of Registration</td>
						<td><b>'. $regType .'</b></td>
					</tr><tr>
						<td>Name</td><td><b>' . $regName . '</b></td>
					</tr><tr>
						<td>Mailing Address</td><td><b>' . $address . '</b></td>
					</tr><tr>
						<td>Street Address</td><td><b>' . $address2 . '</b></td>
					</tr>
					<tr>
						<td>Phone</td><td><b>' . $phone . '</b></td>
					</tr><tr>
						<td>Birth Date</td><td><b>' . $dob . '</b></td>
					</tr>';

			if($parent != '')
			    $body .= '<tr><td>Parent/Guardian</td><td><b>' . $parent . '</b></td></tr>';
			else
			    $body .= '<tr><td>Contact Name</td><td><b>' . $contactName . '</b></td></tr>
				      <tr><td>Contact Phone</td><td><b>' . $contactPhone . '</b></td></tr>';


			$body .= '<tr>
						<td>Email Address</td><td><b>' . $email . '</b></td>
					</tr>';

			if($cardno != '')
				$body .= '<tr><td>Existing Card Number</td><td><b>' . $cardno . '</b></td></tr>';
			else if($filename != '')
				$body .= '<tr><td colspan="2" style="text-align:center;"><b>Identification Attached ('.$filename.')</b></td></tr>';
			else
				$body .= '<tr><td colspan="2" style="text-align:center;"><b>No card number or image included with registration.</b></td></tr>';

			$body .= '</table>';

			if(isset($signedUpFor)) {

			    $body .= '<p style="font-size:.9em;">This user signed up for: ';

			    foreach($signedUpFor as $l)
				$body .= $l . ', ';

			    $body .= '</p>';

			}

			$body .= '</body></html>';


		if($filename != '')
			$attachments = array($_FILES['regIdUpload']['tmp_name'],$filename);
		else
			$attachments = array();

		$result = wp_mail($to,$subject,$body,$headers,$attachments);

		if($result) {
		    $actionresponse .= '<p class="success">Your registration was successfully submitted . Thank you for your interest in our library services&mdash;you will be contacted as soon as possible regarding your registration status. ' . $mailingListMessage . '</p>';
		    unset($regName,$address,$phone,$dob,$email,$address,$address2);
		} else {
		    $actionresponse .= '<p class="error">Error! There was an unknown error sending your message. Please try again later. ' . $mailingListMessage . '</p>';
		}
	}
    $actionresponse .= '</div>';
} // end form submitted

get_header(); ?>

<div id="middle">

    <div id="main" role="main">

    <script type="text/javascript">
    jQuery(function($){

	$('#answerFirstThreeRegQuestions').click(function(){

	    if( $('input[name="regAge"]:checked').length == 0 ||
		    $('input[name="regHaveCard"]:checked').length == 0 ||
		    $('input[name="regType"]:checked').length == 0
	       ) {
		$('#firstThreeRegQuestions').prepend(
		    '<p class="error">Please answer all three questions.</p>');
	    } else {
		$('#firstThreeRegQuestions').slideUp('fast');
		$('#secondRegQuestions').slideDown('fast');
		$('#firstThreeRegQuestions p.error').remove();

	    }
	    return false;

	});



	$('#resetFirstThreeRegQuestions').click(function(){

	    $('#firstThreeRegQuestions').slideDown('fast');
	    $('#secondRegQuestions').slideUp('fast');

	    return false;
	});

	$('input[name="regAge"]').change(function(){

	    var age = $(this).val();

	    if( age == 'ageKid' || age == 'ageTeen') {
		$('#regContactWrap').hide();
		$('#regParentWrap').show();
	    }
	    if( age == 'ageAdult' ) {
		$('#regParentWrap').hide();
		$('#regContactWrap').show();
	    }
	});

	$('input[name="regHaveCard"]').change(function(){

	    var card = $(this).val();

	    if( card == 'haveCardYes') {
		$('#regIdUploadWrap').hide();
		$('#regCardNumberWrap').show();
	    }
	    if( card == 'haveCardNo' || card == 'haveCardNew') {
		$('#regIdUploadWrap').show();
		$('#regCardNumberWrap').hide();

	    }

	});

    });



</script>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


	<h1><?php the_title(); ?></h1>

	<?php if(isset($actionresponse)) echo $actionresponse; ?>

	<?php the_content(); ?>

	<form enctype="multipart/form-data" id="registrationForm" class="pageForm lookForPrefill" method="post" action="">
	    <div id="firstThreeRegQuestions">
		<p><b>To get started, please answer these three questions.</b></p>
		<p><b>Please note, all children under 12 years of age must come into the library with a parent or guardian in order to sign up for a library card.</b></p>

		<span class="required">1. About You</span>
		<div class="choicesBlock">
		    <label for="regLewiston">
		    	<input <?php jpIfSel($_POST['regType'],'Lewiston resident'); ?> type="radio" name="regType" id="regLewiston" value="Lewiston resident" /> 
		    	Lewiston Resident
		    </label>
			<label for="regAuburn">
				<input <?php jpIfSel($_POST['regType'],'Auburn resident'); ?> type="radio" name="regType" id="regAuburn" value="Auburn resident" /> 
				Auburn Resident
			</label>
			<label for="regNonResident">
				<input <?php jpIfSel($_POST['regType'],'non-resident') ?> type="radio" name="regType" id="regNonResident" value="Non-resident" />
				Non-resident
			</label>
			<label for="regNonResidentEmployee">
				<input <?php jpIfSel($_POST['regType'],'Non-resident Employee'); ?> type="radio" name="regType" id="regNonResidentEmployee" value="Non-resident Employee" /> 
				Non-resident (City of Lewiston Employee)
			</label>
			<label for="regNonResidentStudent">
				<input <?php jpIfSel($_POST['regType'],'Non-resident Student'); ?> type="radio" name="regType" id="regNonResidentStudent" value="Non-resident Student" /> 
				Non-resident (student at a school in Lewiston)
			</label>
		</div>

		<span class="required">2. What is your age?</span>
		<div class="choicesBlock">
			<label for="regAgeTeen"><input <?php jpIfSel($_POST['regAge'],'ageTeen'); ?> type="radio" name="regAge" id="regAgeTeen" value="ageTeen" /> 13 - 17</label>
			<label for="regAgeAdult"><input <?php jpIfSel($_POST['regAge'],'ageAdult'); ?> type="radio" name="regAge" id="regAgeAdult" value="ageAdult" /> 18 or older</label>
		</div>

		<span class="required">3. Do you have a library card?</span>
		<div class="choicesBlock">
			<label for="regHaveCardYes"><input <?php jpIfSel($_POST['regHaveCard'],'haveCardYes'); ?> type="radio" name="regHaveCard" id="regHaveCardYes" value="haveCardYes" /> Yes, and I know the number</label>
			<label for="regHaveCardNo"><input <?php jpIfSel($_POST['regHaveCard'],'haveCardNo'); ?> type="radio" name="regHaveCard" id="regHaveCardNo" value="haveCardNo" /> Yes, but I don't know the number</label>
			<label for="regHaveCardNew"><input <?php jpIfSel($_POST['regHaveCard'],'haveCardNew'); ?> type="radio" name="regHaveCard" id="regHaveCardNew" value="haveCardNew" /> No, I've never had a card</label>
		</div>

		<a class="goButton" href="#" id="answerFirstThreeRegQuestions">Next Step &raquo;</a>

	    </div><!--first3 questions-->

	    <div id="secondRegQuestions">

		<a class="goButton" href="#" id="resetFirstThreeRegQuestions">&laquo; Go Back</a>

		<input type="text" value="" id="salutation" name="salutation" />

		<label for="regName" class="required">Name</label>
		    <i>Include your middle initial</i>
		<input required="required" name="regName" type="text" value="<?php if(isset($regName)) echo $regName; ?>" />

		<div id="regCardNumberWrap">
			<!--I gave it the class required to encourage people to read this fully, but it's not REALLY required-->
			<label for="cardno" class="required">Card Number </label>
			<input maxlength="14" name="cardno" type="text" value="<?php if( isset( $cardno ) ) echo $cardno; ?>" />
		</div>

		<label for="regAddress" class="required">Mailing Address</label>
		<textarea required="required" name="regAddress" rows="2" cols="25"><?php if(isset($address)) echo $address; ?></textarea>

		<label for="regAddress2">Street Address (if different)</i></label>
		<textarea name="regAddress2" rows="2" cols="25"><?php if(isset($address2)) echo $address2; ?></textarea>

		<label class="required" for="regPhone">Phone Number</label>
		<input required="required" name="regPhone" type="text" value="<?php if(isset($phone)) echo $phone; ?>" />

		<label for="regDob" class="required">Birth Date</label>
		<input required="required" name="regDob" type="text" value="<?php if(isset($dob)) echo $dob; ?>" />

		<label for="regEmail" class="required">Email</label>
		<input required="required" name="regEmail" type="email" value="<?php if(isset($email)) echo $email; ?>" />

		<div id="regParentWrap">
			<label for="regParent" class="required">Parent/Guardian Name</label>
			<input name="regParent" type="text" value="<?php if(isset($parent)) echo $parent; ?>" />
		</div>

		<div id="regContactWrap">
		    <label for="regContactName" class="required">Secondary Contact</label>
		    <i>Please list someone who does not live with you</i>
		    <label>Name</label>
		    <input name="regContactName" type="text" value="<?php if(isset($contactName)) echo $contactName; ?>" />
		    <label>Phone</label>
		    <input name="regContactPhone" type="text" value="<?php if(isset($contactPhone)) echo $contactPhone; ?>" />
		</div>

		<div id="regMailingListSignupsWrap">

		    <fieldset>
				<legend>Signup for our Mailing Lists?</legend>

				<label for="regMailingListSignups">
				    <input type="checkbox" <?php jpIfChe('salutation','regMailingLewistonAdults'); ?> name="regMailingListSignups[]" value="regMailingLewistonAdults"> Adult Events
				</label>

				<label for="regMailingListSignups">
				    <input type="checkbox" <?php jpIfChe('salutation','regMailingLewistonTeens'); ?> name="regMailingListSignups[]" value="regMailingLewistonTeens" /> Teen Events</label>

				<label for="regMailingListSignups">
				    <input type="checkbox" <?php jpIfChe('salutation','regMailingLewistonKids'); ?> name="regMailingListSignups[]" value="regMailingLewistonKids" /> Children's Events
				</label>

		    </fieldset>
		</div>

		<div class="g-recaptcha" data-sitekey="6LftFtoZAAAAANAKsTmbjAp0O4Tcb9Y8skrA4Ljx"></div>

		<input type="submit" value="Submit" name="submit">

	</div>

	</form>



    <div class="meta">

	<p class="breadcrumbs"><b>You are Here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>

    <?php endwhile; endif; ?>

    </div>


    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>