<?php
/*
Template Name: Contact Form
*/

if(isset($_POST['salutation'])) {

    if($_POST['salutation'] != '') {
	// DO NOTHING
	// HONEYPOT CATCHING BOT
    } else {

    $body = '';
	$actionresponse = '<div id="message">';

	$helpFormName = strip_tags($_POST['helpFormName']);
	$helpFormContact = strip_tags($_POST['helpFormContact']);
	$helpFormCardno = strip_tags($_POST['helpFormCardno']);
	$message = strip_tags($_POST['helpFormQuestion'],'<b><i><s>');

	setcookie('patronName',$helpFormName, time()+60*60*24*30*6, '/'); // 6 months
	setcookie('patronContact',$helpFormContact, time()+60*60*24*30*6, '/'); // 6 months
	setcookie('patronCardno',$helpFormCardno, time()+60*60*24*30*6, '/'); // 6 months

	if($helpFormName == '' || $helpFormContact == '') {
	    $actionresponse .= '<p class="error">You missed a required field. Please fill in your name &amp; either an email address or phone number.</p>';
	} else if($message == '') {
	    $actionresponse .= '<p class="error">You forgot to fill in a question/comment.  :)</p>';
	} else {

		if(strpos($helpFormContact,'@') !== FALSE && strpos($helpFormContact,'.') !== FALSE) {
			$headers = array("Reply-To: $helpFormContact");
			$body .= '<p style="color:gray;"><i>Replies to this email will go to the sender.</i></p>';
		} else {
		    $headers = array("Reply-To: $to");
		}

	    $body .= '<p style="margin-left:20px;">' . stripslashes($message) . '</p>
		<ul><li>Name: <b>' . $helpFormName . '</b></li>
		<li>Contact information: <b>' . $helpFormContact . '</b></li>';

	    if($helpFormCardno != '') $body .= '<li>Library Card: <b>' . $helpFormCardno . '</b></li>';

	    $body .= '
		</ul>';

		$result = wp_mail('general@jake.paris',"email from LPL Website Contact Form ($helpFormName)",$body, $headers);

	    if($result) {
		    $actionresponse .= '<p class="success">Your message was sent successfully. Thank you.</p>';
	    } else {
		    $actionresponse .= '<p class="error">Error! There was an unknown error sending your message. Please try again later.</p>';
	    }

	}
	$actionresponse .= '</div>';
    }


} // end form submitted

get_header(); ?>

<div id="middle">

    <div id="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


	<h1><?php the_title(); ?></h1>

	<?php if(isset($actionresponse)) echo $actionresponse; ?>
	
	 <p><b>Looking for Research?</b> If you do not have a library card and need research help, please use the <a href="<?php echo SITEHOME; ?>/wp-content/uploads/2013/06/Research-Request-Form.pdf">Research Request Form</a> instead. </br>
		 </br>
		 <b>Want to Request an Item?</b> <a href="http://lplonline.org/services/how-to/">Learn how to request an item</a> through our library catalog. To request the purchase of an item not available at Maine libraries, please use the <a href="http://lplonline.org/collection/request-purchase/">Request Purchase</a> form instead.</p>

	<form id="pageContactForm" class="pageForm lookForPrefill" method="post" action="">
	    <input type="text" name="salutation" id="salutation" value="">
			    		 
	  <label class="required">Name</label>
	    <input type="text" name="helpFormName" value="" class="ajaxFill">

	    <label class="required">Contact info (email or phone)</label>
	    <input type="text" name="helpFormContact" value="" class="ajaxFill" >

	    <label>Library Card Number</label>
	    <input type="text" name="helpFormCardno" value="" class="ajaxFill" >

	    <label class="required">Question</label>
	<textarea name="helpFormQuestion"></textarea>
	    <input type="submit" value="Send Form" name="helpFormSubmit">


	</form>


	<?php the_content(); ?>

    <div class="meta">

	<p class="breadcrumbs"><b>You are Here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>

    <?php endwhile; endif; ?>

    </div>


    <?php get_sidebar('mailinglist'); ?>


</div>

<?php get_footer(); ?>
