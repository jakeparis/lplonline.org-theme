<?php
/*
Template Name: Kids
*/
get_header('kids'); ?>

<div id="middle">

    <div id="main" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post();

    ?>

	<p class="breadcrumbs"><b>You are here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>


    <div class="meta">


	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>
    <?php endwhile; endif; ?>



    </div>


    <?php get_sidebar('kids'); ?>


</div>

<?php get_footer('kids'); ?>