<?php
/*
Template Name: Material Request Form
*/

if(isset($_POST['salutation'])) {

    if($_POST['salutation'] != '') {
	// DO NOTHING
	// HONEYPOT CATCHING BOT
    } else {

	$actionresponse = '<div id="message">';

	$itemTitle = strip_tags($_POST['itemTitle']);
	$itemAuthor = strip_tags($_POST['itemAuthor']);
	$itemDate = strip_tags($_POST['itemDate']);
	$itemType = strip_tags($_POST['itemType']);
	$requestFormType = strip_tags($_POST['requestFormType']);

	$requestFormComments = strip_tags($_POST['requestFormComments']);
	$requestFormName = strip_tags($_POST['requestFormName']);
	$requestFormContact = strip_tags($_POST['requestFormContact']);
	$requestFormCardno = strip_tags($_POST['requestFormCardno']);


	if($requestFormName == '' || $requestFormContact == '' || $itemTitle == '') {
	    $actionresponse .= '<p class="error">You missed a required field. Please fill in at least an item title, your name, and a way to contact you.</p>';
	} else {

		$headers = array();
		if(strpos($requestFormContact,'@') !== FALSE && strpos($requestFormContact,'.') !== FALSE) {
			$headers = array("Reply-To: $requestFormContact");
			$body .= '<p style="color:gray;"><i>Replies to this email will go to the sender.</i></p>';
		}

	    $body .= '
		<table>
		    <tr>
			<td style="background:#ddd;"><b>Item Information</b></td>
			<td style="background:#ddd;">&nbsp;</td>
		    </tr><tr>
			<td>Title</td>
			<td><b>' . $itemTitle . '</b></td>
		    </tr><tr>
			<td>Author</td>
			<td><b>' . $itemAuthor . '</b></td>
		    </tr><tr>
			<td>Date</td>
			<td><b>' . $itemdate . '</b></td>
		    </tr><tr>
			<td>Material Type</td>
			<td><b>' . $itemType . '</b></td>
		    </tr><tr>
			<td>Request Type</td>
			<td><b>' . $requestFormType . '</b></td>
		    </tr><tr>
			<td>Comments</td>
			<td><b>' . $requestFormComments . '</b></td>
		    </tr><tr>
			<td style="background:#ddd;"><b>Patron Information</b></td>
			<td style="background:#ddd;">&nbsp;</td>
		    </tr><tr>
			<td>Name</td>
			<td><b>' . $requestFormName . '</b></td>
		    </tr><tr>
			<td>Contact</td>
			<td><b>' . $requestFormContact . '</b></td>
		    </tr><tr>
			<td>Card #</td>
			<td><b>' . $requestFormCardno . '</b></td>
		    </tr>
		</table>
		<p style="font-size:.9em;"><i>Request made ' . date('l, F j, Y') . '</i></p>
		';

	    /* */
		$result = wp_mail(FORMRECIPIENT,"Item Request Form ($requestFormName)",$body,$headers);

	    if($result) {
		    $actionresponse .= '<p class="success">Your request was sent successfully. We will be in touch shortly.</p>';
	    } else {
		    $actionresponse .= '<p class="error">Error! There was an unknown error sending your request. Please try again later.</p>';
	    }

	}
	$actionresponse .= '</div>';
    }

} // end form submitted

get_header(); ?>

<div id="middle">

    <div id="main" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


	<h1><?php the_title(); ?></h1>

	<?php if(isset($actionresponse)) echo $actionresponse; ?>

	<?php the_content();

	?>

	<form id="itemRequestForm" class="pageForm" method="post" action="">
	    <input type="text" name="salutation" id="salutation" value="">

	    <h2>Item Information</h2>
	    <p>Please fill in as much information as you know, but don't worry if you only know some of it.</p>

	    <div id="ajaxFoundBook" ></div>

	    <label class="required" for="itemTitle">The Title</label>
	    <input type="text" id="itemTitle" name="itemTitle" required="required" value="<?php jpIfSet('itemTitle'); ?>">

	    <label for="itemAuthor" class="required">The Author or Creator</label>
	    <input required="required" type="text" id="itemAuthor" name="itemAuthor" value="<?php jpIfSet('itemAuthor'); ?>">

	    <label for="itemDate">The Published Date</label>
	    <input type="text" name="itemDate" id="itemDate" value="<?php jpIfSet('itemDate'); ?>">

	    <label class="required" for="itemType">Type of Item?</label>
	    <select name="itemType" id="itemType">
		<optgroup label="Book">
		    <option value="Regular Book" <?php jpIfSel('itemType','Regular Book'); ?>>Book</option>
		    <option value="Large Print Book" <?php jpIfSel('itemType','Large Print Book'); ?>>Large Print Book</option>
		    <option value="e-Book" <?php jpIfSel('itemType','e-Book'); ?>>e-Book</option>
		</optgroup>
		<optgroup label="Non-Book">
		    <option value="audiobook" <?php jpIfSel('itemType','audiobook'); ?>>Audiobook</option>
		    <option value="dvd" <?php jpIfSel('itemType','dvd'); ?>>DVD</option>
		    <option value="cd" <?php jpIfSel('itemType','cd'); ?>>CD</option>
		</optgroup>
	    </select>

	    <label for="requestFormComments">Additional Comments?</label>
	    <textarea id="requestFormComments" name="requestFormComments"><?php jpIfSet('requestFormComments'); ?></textarea>
	    <br>
	    <h3>Your Information</h3>

	    <label class="required" for="requestFormName">Name</label>
	    <input type="text" id="requestFormName" name="requestFormName" value="<?php jpIfSet('requestFormName'); ?>" >

	    <label class="required" for="requestFormContact">Email address</label>
	    <input type="text" required="required" id="requestFormContact" name="requestFormContact" value="<?php jpIfSet('requestFormContact'); ?>">

	    <label for="requestFormCardno">Library Card Number</label>
	    <input type="text" id="requestFormCardno" name="requestFormCardno" value="<?php jpIfSet('requestFormCardno'); ?>" >

	    <label for="requestType">Is this only a recommendation or do you wish to borrow the item?</label>
	    <p>
	    <input type="radio" name="requestFormType" value="Want to borrow" <?php jpIfChe('requestFormType','Want to borrow'); ?> aria-label="I would like to borrow the item"> I would like to borrow the item.<br>
	    <input type="radio" name="requestFormType" value="Recommendation only" <?php jpIfChe('requestFormType','Recommendation only'); ?> aria-label="This is only a recommendation"> This is only a recommendation.
	    </p>
	    <input type="submit" value="Make Request" name="requestFormSubmit">


	</form>

    <div class="meta">

	<p class="breadcrumbs"><b>You are Here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>

    <?php endwhile; endif; ?>

    </div>


    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>