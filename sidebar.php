<div class="sidebar" aria-label="Sidebar" role="complementary">
	<a class="buttonLink" href="/connect/contact">Contact Us</a>

	<?php

	if( defined('SHOW_SUBPAGE_LIST') ) {

		echo '<ul class="sectionPageList">
	   			<li>In this Section</li>';
			wp_list_pages( array(
			    'sort_column' => 'menu_order,post_title',
			    'child_of' => $post->ID,
			    'title_li' => ''
	    	) );
	    	echo '</ul>';
	}

	if( defined('SHOW_BOOKLETTERS_LIST') ) {

		echo '<ul class="sectionPageList">
			<li>All Newsletters</li>';
		
		    $bl = jpGetBookLetters();
		    foreach($bl as $l) {
				echo '<li><a href="' . $l['permalink'] . '">' . $l['title'] . '</a></li>' . "\n";
		    }
		
	    echo '</ul>';
	    
	}

	?>
</div>

<?php


    if(is_numeric($post->ID)) {
	$dontFeatureThisPage = "AND ".$wpdb->posts.".ID != '".$post->ID."'";
    }

    //*************** FEATURED PAGES ***********
    $featured = $wpdb->get_results(
	"SELECT ".$wpdb->posts.".post_title as title,
	".$wpdb->posts.".ID,
	".$wpdb->posts.".post_content AS content,
	".$wpdb->posts.".post_type,
	".$wpdb->posts.".post_date
	FROM ".$wpdb->postmeta.", ".$wpdb->posts."
	WHERE ".$wpdb->posts.".ID = ".$wpdb->postmeta.".post_id
	AND meta_key = 'feature page'
	AND ".$wpdb->posts.".post_status='Publish'
	$dontFeatureThisPage
	ORDER BY rand() LIMIT 2;"
	);

    if ($featured) : ?>

	<div id="suggestedContent" role="complementary">
	<h2>Other Interesting Pages</h2>

	<?php

	foreach($featured as $un) {

	    $permalink = get_permalink($un->ID);

	    echo '<div class="suggestedSingle">

		<p>';
		if($thumb = jp_get_featured_image($un->ID,'thumb'))
		    echo '<a href="'.$permalink.'" thumb="thumb!">'.str_replace(array(' alt="" ','<img src'),array(' alt="thumbnail" ', '<img alt="thumbnail" src'),$thumb).'</a> ';
		else
		    echo '<a href="'.$permalink.'"><img alt="thumbnail" src="'.THEMEDIR.'/i/logoThumb-2021.png"></a>';

		echo ' <a href="'.$permalink.'">'.$un->title.'</a>';

		if( $un->post_type == 'event')
		    echo ' &mdash; <b>' . getEventDateJp('M d, Y',$un) . '</b><br>';

		else if( $un->post_type == 'post')
		    echo '<br><i>' . date('M d, Y',strtotime($un->post_date)) . '</i> &mdash; ';

		else
		    echo '<br>';

		list($excerpt) = str_split( strip_tags($un->content), 150 );
		if(strlen($un->content) > 150)
		    $excerpt .= '...';

		echo $excerpt .'</p>
	    </div>';
	}

	echo '</div>';

    endif;
    ?>
