jQuery(function($){

    //var url = window.location.href;

    // add beta site contact element
    //var betaFeedback = '<div style="white-space:nowrap;box-shadow:0px 2px 2px #000;z-index:99;position:fixed;height:40px;line-height:40px;fixed;top:0;width:100%;padding:0 20px;background:#f7e657;color:#000;font-size:.9em;">Please <a style="color:#000;" href="mailto:jparis@lewistonmaine.gov">let us know</a> of any problems you come across while testing our new site. Thank you! We appreciate your time.</div>';
    //$('body').prepend(betaFeedback).css('margin-top','40px');


    $('#hideAllMenusButton').click(function(){

	if($(this).data('state') == 'hidden') {

	    $('#topLinks, #topSectionButtons, #mainNav > li').slideDown();

	    $(this).data('state','visible').text('re-hide menus');

	    // use this if we don't need a toggler
	    // $(this).remove();

	    // the hidden state link is sending us to #mainNav, so don't need this:
	    // return false;

	} else {

	    // this line shuts all menus
	    $('#topLinks, #topSectionButtons, #mainNav > li').slideUp();
	    $(this).data('state','hidden').text('show all navigation menus');

	    return false;
	}

    });

    $('.lookForPrefill input[type="text"]').each(function(){
	var prefill = $(this).data('prefill');

	if(prefill) { // if not set, will returned
	    $(this).css({
		"color": "gray",
		"font-style": "italic"
	    })
	    .val(prefill);

	    $(this).on( "focus", function() {
		if($(this).val() == prefill) {
		    $(this).val('');
		}

		$(this).css({
		"color": "#000",
		"font-style": "normal"
		});
	    })
	    .on( "blur", function() {
		if($(this).val() == '') {
		    $(this).css({
			"color": "gray",
			"font-style": "italic"
		    })
		    .val(prefill);
		}
	    });
	}
    });


    $('#mainNav ul ul li').hover(function(){
	if($(this).find('a[href="#"]').length == 0) {
	    $(this).addClass('current-menu-item');
	}
    }, function(){
	if($(this).find('a[href="#"]').length == 0) {
	    $(this).removeClass('current-menu-item');
	}
    });
    $('#mainNav ul ul li').not('.openInNewTab').click(function(){
	loc = $(this).find('a').attr('href');
	window.location = loc;
	});

    if($('.current-menu-item, .current-menu-parent, .current-menu-ancestor').length > 0 ) {
	$('#mainNav li').removeClass('current-page-ancestor');
    }

    // if we're on a page in the menu, allow space for submenu to stay open
    if($('.current-menu-item, .current-page-parent, .current-page-ancestor').length > 0 ) {

	//check to make sure we're not on a tablet or smaller
	if($(window).width() > 799) {

	    // if($('#mainNav > .current-menu-item').length > 0) {
		// var ht = $('.current-menu-item > ul').height() - 3;
		// $('#middle').css('margin-top',ht);
	    // } else if($('#mainNav > .current-menu-ancestor').length > 0) {
		// var ht = $('#mainNav .current-menu-ancestor > ul').height() - 3;
		// $('#middle').css('margin-top',ht);
	    // } else {
		// var ht = $('#mainNav .current-page-ancestor > ul').height() - 3;
		// $('#middle').css('margin-top',ht);
	    // }

	}

    }

    $('#mainNav a[href="#"]').click(function(){
	return false;
	});


    /*
    // Top Search Box Functions
    $('#topSearch').click(function(){
	$(this).find('input[name="s"]').focus();
    });

    $('#topSearch input[name="s"]').focus(function(){
	var osf = $('#openSearchFeatures');

	if(osf.data('liven') != 1) {
	    osf.fadeIn(400,function(){
		$('#openSearchFeatures a').css('color','red');
		osf.fadeOut(400,function(){
		    osf.fadeIn();
		    $('#openSearchFeatures a').css('color','#0073a0');
		});

	    });
	    osf.data('liven',1);
	}

    });

    $('#openSearchFeatures a, #hideSearchFeatures').click(function(){

	$('#searchFeatures').slideToggle('fast');
	return false;

    });

    // hide or show catalog-specific search features
    $('#searchFeatures input[value="catalog"]').change(function(){
	if( $(this).is(':checked') ) {
	    $('#catalogSearchFeatures').slideDown('fast');
	} else {
	    $('#catalogSearchFeatures').slideUp('fast');

	}
    });

    // hilite the checked boxes & labels
    $('input[type="radio"], input[type="checkbox"]').change(function(){

	if( $(this).attr('type') == 'radio' ) {
	    // if this is a radio, unhilite siblings
	    var fName = $(this).attr('name');
	    $('input[name="'+fName+'"]').parent('label').removeClass('hilite');
	}

	if( $(this).is(':checked') ) {
	    $(this).parent('label').addClass('hilite');
	} else {
	    $(this).parent('label').removeClass('hilite');

	}


    });

    // uncheck all types when checking a specific type
    $('#searchTypesWrap input').not('#catalogSearchAllTypes').change(function(){
	$('#searchTypesErr').remove();
	$('#catalogSearchAllTypes').removeAttr('checked');
	    $('#catalogSearchAllTypes').parent('label').removeClass('hilite')
	});
    $('#catalogSearchAllTypes').change(function(){
	// if we select all types, erase the others

	if($(this).is(':checked')) {
	    $('#searchTypesErr').remove();
	    $('#searchTypesWrap input').not('#catalogSearchAllTypes').removeAttr('checked');
	    $('#searchTypesWrap input').not('#catalogSearchAllTypes').parent('label').removeClass('hilite');
	}
    });

    $('#catalogSearchAllFields').change(function(){
	// if we select all types, erase the others
	if($(this).is(':checked')) {
	    $('#searchFieldsErr').remove();
	    $('#searchFieldsWrap input').not('#catalogSearchAllFields').removeAttr('checked');
	    $('#searchFieldsWrap input').not('#catalogSearchAllFields').parent('label').removeClass('hilite');
	}
    });
    // uncheck all types when checking a specific type
    $('#searchFieldsWrap input').not('#catalogSearchAllFields').change(function(){
	$('#searchFieldsErr').remove();
	$('#catalogSearchAllFields').removeAttr('checked');
	    $('#catalogSearchAllFields').parent('label').removeClass('hilite')
	});

    $('#searchWhereWrap input').change(function(){
        $('#searchWhereErr').remove();
    });

     $('#searchScopeWrap input').change(function(){

	if($(this).is(':checked')) {
	    $('#searchScopeWrap input').not(this).parent('label').removeClass('hilite');
	}

    });

    $('#searchAvailLimit input').change(function(){

	if($(this).is(':checked')) {
	    $('#searchAvailLimit input').not(this).parent('label').removeClass('hilite');
	}

    });

    // if neither website or catalog is selected
    $('#topSearch form').submit(function(){
	if($('#searchWhereWrap input').is(':checked')) {

	    $('#searchWhereErr').remove();

	} else {

	    $('#searchWhereWrap').append('<p id="searchWhereErr" style="padding: 5px;background: #ffd4d4;font-weight:bold;color: #5a0000;">Please select where to search!</p>');
	    return false;
	}
	});

    $('#topSearch form').submit(function(){
	    if($('#searchFieldsWrap input').is(':checked')) {

		$('#searchFieldsErr').remove();

	    } else {

		$('#searchFieldsWrap').append('<br><p id="searchFieldsErr" style="padding: 5px;background: #ffd4d4;font-weight:bold;color: #5a0000;">Please select what fields to search!</p>');
		return false;
	    }
	    });


    $('#topSearch form').submit(function(){
	if($('#searchTypesWrap input').is(':checked')) {

	    $('#searchTypesErr').remove();

	} else {

	    $('#searchTypesWrap').append('<br><p id="searchTypesErr" style="padding: 5px;background: #ffd4d4;font-weight:bold;color: #5a0000;">Please select what type of material to search!</p>');
	    return false;
	}
	});
	*/

    $('textarea[name="helpFormQuestion"]').focus(function(){

	$(this).parent('form').find('#helpFormMailingLists').slideDown();

    });


    $('.filterEventsLinks a').click(function(){

	$('p#dynNoEventsFound').remove();
    $('.eventWrap').hide();
	$('#eventsCol .ajaxLoader').show();

	    $(this).addClass('filterEventsLinksOn');
	    $(this).siblings().removeClass('filterEventsLinksOn filterEventsLinksInitial');

	    var cat = $(this).data('category');

	    $.ajax({
		type: "GET",
		url: SITEHOME,
		data: "lplAjax=1&getEvents=true&category="+cat,
		success: function(text) {
		    $('#eventsCol .ajaxLoader').hide();
		    $('#eventsCol').append(text);

		}
	    });
		return false;


    });

    if($('#bookCarousel').length > 0) {

	$.ajax({
	    type: "GET",
	    url: SITEHOME,
	    data: "lplAjax=1&printBookCarousel=true",
	    success: function(books) {
		$('#bookCarousel').append(books);
		$('#bookCarousel p').not('#seeMoreCarousel').remove();
		$('#bookCarousel .ajaxLoader').remove();
	    }
	});

    }


    $('input.ajaxFill').each(function(){

	var me = $(this);
	var name = me.attr('name');

	$.ajax({
	    type: "GET",
	    url: THEMEDIR + "/ajax.php",
	    data: "name="+name+"&fillContactInput=true",
	    success: function(text) {
		me.val(text);
	    }
	});
	return false;

	});



    $('#helpFormSidebar').submit(function(){

	$(this).find('.error').remove();

	var hName = $('#helpFormSidebar input[name="helpFormName"]').val();
	    hName = $('<div/>').text(hName).html();

	var hContact = $('#helpFormSidebar input[name="helpFormContact"]').val();
	    hContact = $('<div/>').text(hContact).html();

	var hQuestion = $('#helpFormSidebar textarea[name="helpFormQuestion"]').val()
	    hQuestion = $('<div/>').text(hQuestion).html();;

	var hSalutation = $('#helpFormSidebar input[name="salutation"]').val();
	    hSalutation = $('<div/>').text(hSalutation).html();

	var mailingListAdults = '',
	    mailingListTeens = '',
	    mailingListKids = '';

	if( $('#helpFormSidebar input[name="mailingListAdults"]').is(':checked') ) var mailingListAdults = '&mailingListAdults=true';
	if( $('#helpFormSidebar input[name="mailingListTeens"]').is(':checked') ) var mailingListTeens = '&mailingListTeens=true';
	if( $('#helpFormSidebar input[name="mailingListKids"]').is(':checked') ) var mailingListKids = '&mailingListKids=true';

	// security

	//validate
	if( hName == '' || hContact == '' || hQuestion == '') {

	    if( hName == '')
		$('#helpFormSidebar input[name="helpFormName"]').css('border','1px solid red').after('<span class="error">Name, please.</span>');
	    else
		$('#helpFormSidebar input[name="helpFormName"]').css('border','none');

	    if( hContact == '')
		$('#helpFormSidebar input[name="helpFormContact"]').css('border','1px solid red').after('<span class="error">Please enter some way to contact you.</span>');
	    else
		$('#helpFormSidebar input[name="helpFormContact"]').css('border','none');

	    if( hQuestion == '')
		$('#helpFormSidebar textarea[name="helpFormQuestion"]').css('border','1px solid red').after('<span class="error">Surely you have something to write here?</span>');
	    else
		$('#helpFormSidebar textarea[name="helpFormQuestion"]').css('border','none');


	// all systems go
	} else {

	    $('#helpFormSidebar :not(.ajaxLoader)').fadeOut('fast');
	    $('#helpFormSidebar .ajaxLoader').fadeIn('fast');

	    var dataString = 'salutation=' + hSalutation + '&hName='+ hName + '&hContact=' + hContact + '&hQuestion=' + hQuestion + mailingListKids + mailingListTeens + mailingListAdults;

	    $.ajax({
		type: "POST",
		url: THEMEDIR + "/submitProcess.php",
		data: dataString,
		success: function(text) {
		    $('#helpFormSidebar').html(text);
		}
	    });
	    return false;


	}
	return false;
    });


    $('#accountLoginForm input[name="name"]').blur(function(){

	$.ajax({
	    type: 'get',
	    url: THEMEDIR + '/ajax.php',
	    data: 'patronName=' + $(this).val(),
	    success: function(txt) {
		$('#accountLoginForm').append(txt);
	    }
	});

    });
    $('#accountLoginForm input[name="code"]').blur(function(){

	$.ajax({
	    type: 'get',
	    url: THEMEDIR + '/ajax.php',
	    data: 'patronCardno=' + $(this).val(),
	    success: function(txt) {
		$('#accountLoginForm').append(txt);
	    }
	});

    });

    $('#itemRequestForm input[name="itemAuthor"], #itemRequestForm input[name="itemTitle"]').blur(function(){

	var itemTitle = $('#itemRequestForm input[name="itemTitle"]').val();
	var itemAuthor = $('#itemRequestForm input[name="itemAuthor"]').val();

	if( itemAuthor != '' && itemTitle != '' ) {

	    $.ajax({
		type: 'get',
		url: THEMEDIR + '/ajax.php',
		data: 'itemTitle=' + itemTitle + '&itemAuthor=' + itemAuthor,
		success: function(txt) {
		    if( txt != 'none') {
			$('#ajaxFoundBook').html(txt).fadeIn('fast');
		    } else
			$('#ajaxFoundBook').fadeOut('fast').html('');
		}
	    });
	}

    });

    $('#closeMatchingItems').on('click',function(){
	$('#ajaxFoundBook').fadeOut('fast').html('');
	return false;
	});


    // LIBRARY VALUE USE CALC
    $('.lvc_userCalcResult').attr('disabled','disabled').css({
	'color':'#000',
	'border':'0px',
	'font-weight':'bold'
	});
    $('#lvc_calculator').css('padding-top','7px');
    $('#lvc_calculator input').css({'padding':'7px 5px','width':'100px'});

    function lvc_updateTotal () {
        var addto = 0;
	$('.lvc_userCalcResult').each(function(){
	    var newadd = $(this).val();
	    addto = addto + (newadd-0);
        });
	addto = addto.toFixed(2);
	$('#lvc_totalResult').val(addto);
    }

    $('#lvc_calculator input[type="text"]').keyup(function(){

	var val = $(this).val();
        var $result = $(this).closest('tr').find('.lvc_userCalcResult');
        var res = val * $result.attr('data-multiplier');
        if(res - 0 == res) { // make sure it's a number
           res = res.toFixed(2);
            $result.val(res);
            lvc_updateTotal();
        }

    });


    $('.resultRow').css('cursor','pointer')
	.hover(function(){
		$(this).find('a:first').css('border-bottom','none');
		$(this).data('origBack',$(this).css('background'));
		$(this).css('background','#F7F2D4');
	    },function(){
		$(this).find('a:first').css('border-bottom','1px solid silver');
		$(this).css('background',$(this).data('origBack'));
	    }
	).click(function(){
	    loc = $(this).find('a:first').attr('href');
	    window.location = loc;
	    });;


        // chat popup
    $('.openChatWindow').click(function(){

	var chatWind = window.open(THEMEDIR + '/chat-window.php','chatwindow','height=600,width=350,scrollbars=no,status=no,resizable=yes,menubar=no');
	    if (chatWind == null || typeof(chatWind)=='undefined') {
	    window.location =  $(this).data('chatPageUrl');
	}

	return false;
    });


    $('a.delete').click(function(){
	if(confirm('Are you sure you want to delete ' + $(this).attr('title') + '?')) {
		var cleanUrl = $(this).attr('href');
		cleanUrl = cleanUrl.replace('#','');
		window.location = $(this).attr('href') + '&delete=confirmed&id=' + $(this).data('id');
		return false;
	} else
		return false;
});


if( $('#topSearch form').length > 0 ) {
    var stickyTop = $('#topSearch form').offset().top;
    var stickyRight = $('#topSearch form').offset().right;
}
$(window).scroll(function(){
    if($(this).width() > 800) { // responsive breakpoint (laptop)
	if ( stickyTop - 10 < $(this).scrollTop() ) {
	    var topW = $('#topSearch').width() - 12;
	    $('#topSearch form').css({ position: 'fixed', top: 10, right: stickyRight, width:topW });
	    $('#searchFeatures').slideUp('fast');

	} else {
	    $('#topSearch form').css({position:'static'});
	}
    }
});

$('.openInNewTab a').each(function(){

    $(this).attr('target','_blank');

    });
    
    
var changeSearchFormTarget = function() {

    $('#topSearch form input[name="viewportWidth"]').val( $(window).width() );

}

changeSearchFormTarget();
$(window).resize(function(){
    changeSearchFormTarget();
});

/* for music cd sampler * /
if (window.HTMLAudioElement) {

    $('#showAudioSample').click(function(){

	$(this).fadeOut();
	$('#viewAuburnHours').fadeTo('fast',0);
	$('#audioPopup').animate({bottom:4});
	var audioPlayer = document.getElementById('audioSamplePlayer');
	audioPlayer.play();

    });

} else {
    $('#audioPopup').remove();
}
*/

});
