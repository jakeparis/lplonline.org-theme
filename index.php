<?php
if(!isset($wpdb)) die;
get_header(); ?>

<div id="middle">

    <div id="main" role="main">


    <?php $post = $posts[0]; // Hack. Set $post so that the_date() works.
    if (is_category()) {
	$pageTitle = 'In the ' . single_cat_title('',false) . ' category';
    } else if(is_tag()) {
	$pageTitle = 'In the ' . single_tag_title('',false) . ' category';
    } else if(is_tax()) {
	$pageTitle = 'In the ' . single_term_title('',false) . ' category';
    } else {
	$pageTitle = 'Category Index';
    }
    ?>

    <h1><?php echo $pageTitle; ?></h1>


    <?php $odd = 'odd';
    if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="resultRow <?php echo $odd; ?>">
	    <?php echo jp_get_featured_image($post->ID,'medium'); ?>
	    <p><b><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></b>
		&nbsp;&nbsp;&nbsp;<span style="color: #555;font-size:.8em;font-style:italic;"><?php if( $post->post_type == 'post' ) echo 'article'; else echo $post->post_type; ?></span>
	    <br>

	    <?php if($post->post_type == 'event') {

		$evdates = mejp_getEventDates();

		if(!empty($evdates)) {
		    $evtime = mejp_getEventTime();

		    echo '<i>';

		    $evdatesStr = implode(', ',$evdates);

		    echo $evdatesStr . '&mdash; ' .$evtime . '</i> &nbsp;';


		}

	    } else if($post->post_type == 'post')
		echo '<i>' . get_the_date() . '</i> &mdash;';
	    ?>

	    <?php
	    list($excerpt) = str_split(	get_the_excerpt() , 200 );
	    echo $excerpt;
	    if(strlen(get_the_excerpt() > 205))
		echo '...';
	    ?>
	    </p>
	    </div>

    <?php
    $odd = ($odd == 'odd') ? '' : 'odd';

    endwhile; endif; ?>

    </div>

    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>