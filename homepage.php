<?php
/*
Template Name: Homepage
*/
get_header();
?>

<div id="content" role="main">
	<?php
		$bans = $wpdb->get_results("SELECT opt_value,opt_option FROM ".$wpdb->prefix."custom_options WHERE opt_name = 'homeBanner'; " );
		foreach($bans as $ban) {
			if($ban->opt_option == '#')
				$bannerUrl = UPLOADSDIR . '/ban/' . $ban->opt_value;
		}
		// /wp-content/themes/cleanGrayLPL/i/hall-bg.jpg
	?>
	<div id="home-banner" data-test="yes" style="background-position: center; background-size: cover; background-image: url('<?php echo $bannerUrl?>');">
	
	<h1 style="margin-left:-999px">Lewiston Public Library</h1>
    </div>
	
	

    <div id="popular-pages" class="" style="overflow:visible;max-height:auto;">
	
	<h5>Popular Pages</h5>

	<?php wp_nav_menu(array('menu'=>'popular-pages','container'=>'','menu_class'=>'popularPages'));	?>


    <?php
    $bans = $wpdb->get_results("SELECT opt_value,opt_option FROM ".$wpdb->prefix."custom_options WHERE opt_name = 'homeBanner'; " );
    if($bans !== 0) { ?>
	<script src="<?php echo THEMEDIR; ?>/lib/bxslider/jquery.bxslider.min.js"></script>
	<script>
	    jQuery(function($){

		$('#homeBanners ul').bxSlider({
		    auto: true,
		    speed: 1100,
		    pause: 7000,
		    autoHover: true,
		    randomStart: true,
		    pager: false,
		    easing: 'ease-out'
		  });

	    });
	</script>
	<div id="homeBanners">
	    <ul>
    <?php
	shuffle($bans);
	foreach($bans as $ban) {
	    echo '<li><a href="' . $ban->opt_option . '"><img src="'. UPLOADSDIR . '/ban/' . $ban->opt_value . '" alt="'.$ban->opt_value.'" /></a></li>' . "\n";
	}
    ?>
	    </ul>
	</div>

    <?php }   ?>

    </div>


    <div class="frontCol">


	<h2 class="sectionHeader"><img src="<?php echo THEMEDIR; ?>/i/icon-calendar.png" alt=""> Events</h2>

	<?php 
	$homepageEventsInitialCat = get_option('lpl_homepage_events_initial_display', 'all');
	// echo $homepageEventsInitialCat;
	$events = [];
	switch($homepageEventsInitialCat){
		case 'adults' :
			$events = mejp_getEvents(8, 'future', 'Adults');
			if( count($events) > 1 ) {
				$adultsFilterClasses = ' filterEventsLinksOn filterEventsLinksInitial ';
			}

			break;
		case 'kids' :
			$events = mejp_getEvents(8, 'future', 'Kids');
			if( count($events) > 1 ) {
				$kidsFilterClasses = ' filterEventsLinksOn filterEventsLinksInitial ';
			}
			break;
		case 'teens' :
			$events = mejp_getEvents(8, 'future', 'Teens');
			if( count($events) > 1 ) {
				$teensFilterClasses = ' filterEventsLinksOn filterEventsLinksInitial ';
			}
			break;
	}

	if( ! count($events) ) {
		$events = mejp_getEvents(25);
		$allFilterClasses = ' filterEventsLinksOn filterEventsLinksInitial ';
	}

	?>

	<p class="filterEventsLinks">Show:
	    <a href="#" data-category="all" class="<?php echo $allFilterClasses ?>">All</a>
	    <a href="#" data-category="adults" class="<?php echo $adultsFilterClasses ?>">Adults</a>
	    <a href="#" data-category="kids" class="<?php echo $kidsFilterClasses ?>">Kids</a>
	    <a href="#" data-category="teens" class="<?php echo $teensFilterClasses ?>">Teens</a>
	</p>


	<img style="position:static;" class="ajaxLoader" src="<?PHP echo THEMEDIR; ?>/i/ajax-loader.gif">
<?php


if ($events) :

	$shown = array();

	foreach($events as $eventDate => $evsForDate) {
		foreach($evsForDate as $evOccurances) {
			foreach($evOccurances as $ev) {

				if($ev->eGroup == 1) {
					// don't show the event multiple times...
					if(in_array($ev->ID,$shown)) continue;
				}

				//get categories
				$catArr = get_the_terms($ev->ID,'event-tag');
				if( $catArr != false && ! is_wp_error($catArr) ) {
				foreach($catArr as &$ct) {
					$ct = 'CAT-'.$ct->slug;
				}
				$catString = implode(' ',$catArr);
				}

				?>
				<div class="singleEvent <?php echo $catString; ?>">
				<a href="<?php echo $ev->guid; ?>"><?php echo jp_get_featured_image($ev->ID); ?></a>
				<div class="item-description">
				<h2 style="font-size:18px;"><a href="<?php echo $ev->guid; ?>"><?php echo $ev->post_title; ?></a></h2>
				
				<p class="eventDate"><?php echo date('M d, Y', strtotime($eventDate)) . ' &mdash; ' . $ev->time .'</p>';

				$showChars = 150;

				$desc = strip_tags($ev->post_content);
				list($desc) = str_split($desc,$showChars);
				echo '<p>' . $desc;
				if(strlen( $ev->post_content ) > $showChars)
					echo '...<a href="' . $ev->guid . '">read more</a>.';

				echo '</p>';
				?>
				</div>
				</div>
				<?php
				$shown[] = $ev->ID;
			}
		}
	}


	else : ?>

	<div class="singleEvent">
	    <p>No events at the moment.</p>
	</div>

	<?php endif;
	/*** END EVENT COLUMN ***/
 ?>

    </div>
	
	<div id="home-calendar-wrap">
	
	
	<h2 class="sectionHeader" style="background:none;"><img src="<?php echo THEMEDIR; ?>/i/icon-calendar.png" alt=""> Events</h2>
	<a class="button" href="/events-programs/events-calendar-view/" style="margin-top: -54px;">More events >></a>
	<?php 
	$homepageEventsInitialCat = get_option('lpl_homepage_events_initial_display', 'all');
	// echo $homepageEventsInitialCat;
	$events = [];
	switch($homepageEventsInitialCat){
		case 'adults' :
			$events = mejp_getEvents(8, 'future', 'Adults');
			if( count($events) > 1 ) {
				$adultsFilterClasses = ' filterEventsLinksOn filterEventsLinksInitial ';
			}

			break;
		case 'kids' :
			$events = mejp_getEvents(8, 'future', 'Kids');
			if( count($events) > 1 ) {
				$kidsFilterClasses = ' filterEventsLinksOn filterEventsLinksInitial ';
			}
			break;
		case 'teens' :
			$events = mejp_getEvents(8, 'future', 'Teens');
			if( count($events) > 1 ) {
				$teensFilterClasses = ' filterEventsLinksOn filterEventsLinksInitial ';
			}
			break;
	}

	if( ! count($events) ) {
		$events = mejp_getEvents(25);
		$allFilterClasses = ' filterEventsLinksOn filterEventsLinksInitial ';
	}

	?>

	<p class="filterEventsLinks">Show:
	    <a href="#" data-category="all" class="<?php echo $allFilterClasses ?>">All</a>
	    <a href="#" data-category="adults" class="<?php echo $adultsFilterClasses ?>">Adults</a>
	    <a href="#" data-category="kids" class="<?php echo $kidsFilterClasses ?>">Kids</a>
	    <a href="#" data-category="teens" class="<?php echo $teensFilterClasses ?>">Teens</a>
	</p>
	<div id="eventsCol" class="eventList" style="display:flex;clear:both">
	<?php


if ($events) :

	$shown = array();

	foreach($events as $eventDate => $evsForDate) {
		foreach($evsForDate as $evOccurances) {
			foreach($evOccurances as $ev) {
		
				if(count($shown) >= 3) {
					continue;
				}

				if($ev->eGroup == 1) {
				// don't show the event multiple times...
				if(in_array($ev->ID,$shown)) continue;
				}

				//get categories
				$catArr = get_the_terms($ev->ID,'event-tag');
				if( $catArr != false && ! is_wp_error($catArr) ) {
				foreach($catArr as &$ct) {
					$ct = 'CAT-'.$ct->slug;
				}
				$catString = implode(' ',$catArr);
				}

				?>
				
				<div class="eventWrap <?php echo $catString; ?>">
				<a href="<?php echo $ev->guid; ?>"><?php echo jp_get_featured_image($ev->ID); ?></a>
				<div class="">
				<h2 style="font-size:18px;"><a href="<?php echo $ev->guid; ?>"><?php echo $ev->post_title; ?></a></h2>
				
				<p class="eventDate"><?php echo date('M d, Y', strtotime($eventDate)) . ' &mdash; ' . $ev->time .'</p>';

				$showChars = 150;

				$desc = strip_tags($ev->post_content);
				list($desc) = str_split($desc,$showChars);
				echo '<p>' . $desc;
				if(strlen( $ev->post_content ) > $showChars)
					echo '...<a href="' . $ev->guid . '">read more</a>.';

				echo '</p>';
				?>
				</div>
				</div>
				
				<?php
				$shown[] = $ev->ID;
			}
		}
	}


	else : ?>

	<div class="singleEvent">
	    <p>No events at the moment.</p>
	</div>
		
	<?php endif;
		/*** END EVENT COLUMN ***/
	?>
	
	</div>
	
	<br><br>
	<h2 class="sectionHeader"><img src="<?php echo THEMEDIR; ?>/i/icon-news.png" alt=""> News</h2>
	<a class="button" style="margin-top:-54px" href="/category/news/">More news >></a>
	<div style="clear: both;"></div>
	<?php
	query_posts('posts_per_page=3&category_name=News');
	if(have_posts()) :
	while (have_posts()) : the_post();
	?>
	<div class="singleNews full-width">
		<a href="<?php the_permalink(); ?>">
			<?php echo jp_get_featured_image($post->ID,'thumbnail');?></a>
			<div class="item-description">
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<p>
		    <?php
			$showChars = 300;

			$oExcerpt = get_the_excerpt();
			list($excerpt) = str_split($oExcerpt,$showChars);
			echo $excerpt;
			if(strlen($oExcerpt) > $showChars) {
			    echo '...<a href="' . get_permalink() . '">read more</a>.';
			}
		    ?>
			</div>
		</p>
	</div>
	<?php endwhile; else : ?>

	<div class="">
		<p>No news at the moment.</p>
	</div>

	<?php endif; ?><br><br>
	<div class="home-tiles">
	<a class="home-tile" href="/services/library-cards/">Library Cards</a>
	<a class="home-tile" href="https://lplonline.org/about/support-the-library/">Support the Library</a>
	<a class="home-tile" href="https://www.maine.gov/governor/mills/lewiston">Healing Together Resources</a>
	</div>
	</div> <!-- end home cal wrap -->
	
    <div class="frontCol">

	<h2 class="sectionHeader"><img src="<?php echo THEMEDIR; ?>/i/icon-news.png" alt=""> <a href="http://lplonline.org/category/news/">Library News</a></h2>

	<?php
	query_posts('posts_per_page=3&category_name=News');
	
	if(have_posts()) :
	//echo '<ul style="margin-left:0;">';
	while (have_posts()) : the_post(); ?>

	<div class="singleNews">
	    

			<a href="<?php the_permalink(); ?>">
			<?php echo jp_get_featured_image($post->ID,'thumbnail');?></a>
			<div class="item-description">
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	    
	    <p>
		    <?php
			$showChars = 175;

			$oExcerpt = get_the_excerpt();
			list($excerpt) = str_split($oExcerpt,$showChars);
			echo $excerpt;
			if(strlen($oExcerpt) > $showChars)
			    echo '...<a href="' . get_permalink() . '">read more</a>.';
		    ?>
		</p>
		</div>
		
	</div>

	<?php endwhile; else : ?>

	<div class="singleNews">
	    <p>No news at the moment.</p>
	</div>

	<?php endif; ?>

    </div>
	

<?php 
if( function_exists('book_carousel_show') ){
    echo '<h2 class="sectionHeader" style="margin-left:20px; float: left;clear:left;margin-bottom: 1px;">New Purchases</h3>
	<a class="button" id="more-purchases" style="margin-right:20px" href="https://lplonline.org/collection/newest-purchases-1/">More new purchases >></a>
		</div>';
	echo book_carousel_show();
} else {
	echo '</div>';
}

?>

<?php

get_footer(); ?>