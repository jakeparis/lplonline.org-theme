<?php

if(isset($_GET['fillContactInput'])) {
    if($_GET['name'] == 'helpFormName') $iname = 'patronName';
    if($_GET['name'] == 'helpFormContact') $iname = 'patronContact';

    if(isset($_COOKIE[$iname]))
	echo $_COOKIE[$iname];

}

if(isset($_GET['patronName'])) {

    $patronName = str_replace('%','',strip_tags($_GET['patronName']));
    setcookie('patronName',$patronName, time()+60*60*24*30*6, '/');

}

if(isset($_GET['patronCardno'])) {

    $patronCardno = str_replace('%','',strip_tags($_GET['patronCardno']));
    setcookie('patronCardno',$patronCardno, time()+60*60*24*30*6, '/');

}

if(isset($_GET['itemAuthor']) && isset($_GET['itemTitle'])) {

    // for some reason urlencode() messes it up
    //$itemAuthor = urlencode($_GET['itemAuthor']);

    $itemAuthor = $_GET['itemAuthor'];


    //reverse the author name
    $itemAuthor = trim($itemAuthor);
    if(strpos($itemAuthor,' ') !== FALSE) {
	$authArr = explode(' ',$itemAuthor);
	$authArr = array_reverse($authArr);
	$itemAuthor = implode(',+',$authArr);
    }

    $itemTitle = urlencode($_GET['itemTitle']);

    //mainecat or minerva?
    $catalogName = 'mainecat';

    // author & title search
    $url = 'http://'.$catalogName.'.maine.edu/search/q?author='.$itemAuthor.'&title='.$itemTitle.'&sortdropdown=-';
    // reg keyword search
    //$url = 'http://mainecat.maine.edu/search/X?SEARCH='.$itemAuthor.'+' . $itemTitle . '&SORT=D&searchscope=71';

    //echo $url;

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $catResults = curl_exec($ch);
    curl_close($ch);


    if(stripos($catResults,'Your limiting title was not found') !== FALSE || stripos($catResults,'No matches found') !== FALSE){
	echo 'none';
	return;
    }

    if(stripos($catResults,'briefcitRow') !== FALSE) {
	// multiple results

	$i = 0;
	$showRows = 6; // limit result set
	//$odd = 'odd';

	// scrape count of results
	if(stripos($catResults,'class="browseSearchtoolMessage"') !== FALSE) {
	    list( ,$resCount) = explode('class="browseHeaderData',$catResults);

	    list($resCount) = explode(')',$resCount);
	    list(,$resCount) = explode('of',$resCount);
	    $resCount = trim($resCount);
	}


	$rows = explode('class="briefcitRow"',$catResults);
	// take off opac header
	array_shift($rows);

	echo '<a id="closeMatchingItems" href="#">X</a>
	<p><b>Matching Items Found!</b><br>
	<p>Please only use this form if <i>no</i> libraries in Maine already own the item. It is much faster to order the item from another library than for us to purchase it.
	' . $resCount . ' items were found in the catalog that may fulfill your request. Please take a look before continuing</p>
	<ul>';

	foreach($rows as $un) {

	    //single row
	    if($showRows == $i) break;
	    //get title

	    $char = stripos($un,'<h2 class="briefcitTitle">');
	    $title = substr($un,$char);

	    list(,$title) = explode('<h2 class="briefcitTitle">',$un);
	    // grab out title w/ link
	    list($title) = explode('</h2>',$title);

	    // absolute-ize the link
	    $title = str_replace('a href="/','a target="catalog" href="http://'.$catalogName.'.maine.edu/',$title);

	    // take out h2
	    $title = strip_tags($title, '<a>');

	    /*
	    // now get the image w/ link
	    $char = stripos($un,'<div class="briefcitJacket">');
	    $img = substr($un,$char);
	    $img = str_replace('<div class="briefcitJacket">','',$img);

	    // grab out image w/ link
	    list($imageTag) = explode('</a>',$img);
	    // take link away
	    $imageTag = trim( strip_tags($imageTag,'<img>') );


	    if($imageTag != '' && $imageTag != '&nbsp;') {

		list(,$imgUrl) = explode('"',$imageTag);

		list($imgUrl) = explode(' ',$imgUrl);

		if($imgUrl != '')
		    list($w) = getimagesize($imgUrl);

		if($w < 2 || !isset($w))
		    $imageTag = '';
	    } else
		$imageTag = '';

	    //print_r($imgUrl);

	    /*

	    // get media type
	    /*
	    list(,$media) = explode('<div class="briefcitMedia"',$un);
	    list(,$media) = explode('alt="',$media);
	    list($media) = explode('"',$media);
	    $media = ucwords(strtolower($media));

	    // get call no
	    list(, $callno) = explode('<!-- field C -->',$un);
	    list($callno) = explode('<!-- field v -->',$callno);
	    $callno = trim( strip_tags($callno) );
	    $callno = str_replace('&nbsp;','',$callno);

	    // get status
	    list(, $status) = explode('<!-- field % -->',$un);
	    list($status) = explode('<!-- field ! -->',$status);
	    $status = trim( strtolower($status) );
	    $status = str_replace('&nbsp;','',$status);
	    */

	    echo '<li>' . $title . '</li>';

	    $i++;
	}
	echo '</ul>';
	if($resCount > $showRows)
	    echo '<p><a href="' . $url . '" target="catalog">See the rest</a>';



    } else if(stripos($catResults,'bibDisplayContentMain') !== FALSE) {
	// single result

	// get the title
	list($null,$title) = explode('<!-- next row for fieldtag=t -->',$catResults);
	list($title) = explode('</tbody>',$title);
	list($null,$title) = explode('<td class="bibInfoData"',$title);
	$title = trim($title, '>');
	list($title) = explode('</td>',$title);
	$title = str_replace('&nbsp;','',$title);
	$title = trim( strip_tags($title) );

	// get image
	list(, $image) = explode('<div class="bibDisplayJacket"',$catResults);
	list(, $image) = explode('<a',$image);
	list(, $image) = explode('<img',$image);
	list($image) = explode('</a>',$image);

	/* // this gets the existing librarything covers, but it don't work
	if($image != '') {
	    list(,$imgUrl) = explode('src="',$image);
	    list($imgUrl) = explode('"',$imgUrl);
	    list($w) = getimagesize($imgUrl);
	}
	*/
	//http://covers.librarything.com/devkey/025281e05f6d3508ad3287cdad0406cd/large/isbn/9781554073009
	if($image != '') {
	    list(,$isbn) = explode('isbn/',$image);
	    $imgUrl = 'http://syndetics.com/index.aspx?isbn='.$isbn.'/SC.GIF&client=mainep&type=rw12';
	    list($w) = getimagesize($imgUrl);
	}


	if($w < 2 || !isset($w))
	    $image = '';

	if($image == '')
	    $image = '';
	else
	    $image = '<a target="_blank" href="'.$url.'"><img' . $image . '</a><br>';

	echo '<a id="closeMatchingItems" href="#">X</a><p><i>Is this the item you were looking for?</i></p>
	    <p>' . $image . '
	    <b>' . $title . '</b></p>
	    <p>If so, <a target="_blank" href="'.$url.'">click here</a> rather than use this form</p>';

    } else {
	echo 'none';
    }

}
