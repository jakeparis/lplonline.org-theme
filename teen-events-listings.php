<?php
/*
Template Name: Teen Events Listings
*/
get_header('teens'); ?>

<div id="middle">

    <div id="main">


    <?php $odd = 'odd';
    if (have_posts()) : while (have_posts()) : the_post(); ?>

	<h1><?php the_title(); ?></h1>

	<?php the_content();


    endwhile; endif;

    // get all teen events
    $events = mejp_getEvents(20, 'future', 'teens');

    if ($events) :

    // reverse the arr to put the most recent at the top
    //$events = array_reverse($events);

    echo '<h2>Upcoming Events</h2>';

    $oldEvents = array();
    $shown = array();
    $i = 0;

	foreach($events as $eventDate => $evsForDate) {
		foreach($evsForDate as $evOccurances) {
			foreach($evOccurances as $ev) {

				// if it's an old events put the event in a different array and
				// move to the next
				if(date('Y-m-d') > $ev->unformattedDate) {
					$oldEvents[] = $ev;
					continue;
				}

				// don't show the event multiple times...
				if($ev->eGroup == 1) {
					if(in_array($ev->ID,$shown))
					continue;
				}

				?>

				<div class="resultRow <?php echo $odd; ?>">
				<?php echo jp_get_featured_image($ev->ID,'medium') . '
				<p><b><a href="'. $ev->guid .'">'. $ev->post_title . '</a></b> &nbsp; &nbsp;
					<i>'. date('M d, Y', strtotime($eventDate)) . ' &mdash; ' . $ev->time . '</i>
					<br>';

				list($excerpt) = str_split(	strip_tags($ev->post_content) , 200 );
				echo $excerpt.'...';
				?>
				</p>
				</div>

				<?php
				$odd = ($odd == 'odd') ? '' : 'odd';
				$i++;

				$shown[] = $ev->ID;
			}
		}
    }

    if($i == 0)
	echo '<p>No upcoming events listed at this time. Please check back later.</p>';

    if (!empty($oldEvents)) {

	// reverse the arr to put the most recent at the top
	$oldEvents = array_reverse($oldEvents);

	echo '<h2>Past Events</h2>';

	foreach($oldEvents as $ev) {

	    // don't show the event multiple times...
	    if($ev->eGroup == 1) {
		if(in_array($ev->ID,$shown)) continue;
	    }

	    ?>

	    <div class="resultRow <?php echo $odd; ?>">
	    <?php echo jp_get_featured_image($ev->ID,'medium') . '
	    <p><b><a href="'. $ev->guid .'">'. $ev->post_title . '</a></b> &nbsp; &nbsp;
		<i>'. $ev->date . ' &mdash; ' . $ev->time . '</i>
		<br>';

	    list($excerpt) = str_split(	strip_tags($ev->post_content) , 200 );
	    echo $excerpt.'...';
	    ?>
	    </p>
	    </div>

	    <?php
	    $odd = ($odd == 'odd') ? '' : 'odd';

	    $shown[] = $ev->ID;
	}

    }
    endif;

   ?>
    </div>

    <?php get_sidebar(); ?>


</div>

<?php get_footer('teens'); ?>