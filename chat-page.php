<?php
/*
Template Name: Chat Page
*/
get_header(); ?>

<div id="middle">

    <div id="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>


    <?php endwhile; endif; ?>

    <div style="max-width:400px;height:350px;">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript" ></script>
    <div id="questionpoint.chatwidget" qwidgetno="1" ></div>
    <script id="questionpoint.bootstrap" qwidgetno="1" type="text/javascript" src="http://www.questionpoint.org/crs/qwidgetV4/js/qwidget.bootstrap.js?langcode=1&instid=13596&skin=black&size=fill" charset="utf-8">
    //<noscript>Please enable javascript to chat with librarians online</noscript></script>
    </div>

    </div><!--#main-->


    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>