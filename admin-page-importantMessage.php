<?php
defined('ABSPATH') || die('Not allowed');

if( isset($_POST['important-message-submit']) ){
	if( ! wp_verify_nonce( $_POST['importantMessage_nonce'], 'save-message' ) ) {
		echo '<div class="message error"><p>Could not update message. Perhaps the form timed out. Try again?</p></div>';
	} else {
		if( empty($_POST['important_message']) )
			delete_option('lpl-important-message');
		else
			update_option('lpl-important-message', stripslashes($_POST['important_message']) );
		
		if( empty($_POST['expires-date']) )
			delete_option('lpl-important-message-expiration');
		else
			update_option('lpl-important-message-expiration', $_POST['expires-date']);

		echo '<div class="message updated"><p>Updated message.</p></div>';
	}
}

$msg = get_option('lpl-important-message','');
$date = get_option('lpl-important-message-expiration','');
?>
	
<div class="wrap">
	<h1>Homepage Important Message</h1>
	<p>Use this page to display the message on the homepage that displays at the top.</p>

	<form action="" method="post">
		<?php wp_nonce_field( 'save-message', 'importantMessage_nonce' ); 

		wp_editor($msg,'important_message',[
			'media_buttons' => false,
			'teeny' => true,
			'quicktags' => false
		]);
		?>

		<h2>Expiration</h2>
		<p>Last day to display this message. Leave blank for no expiration date. </p>
		<input type="date" name="expires-date" value="<?= $date ?>">

		<input type="submit" class="button button-primary" value="Save" name="important-message-submit">
	</form>