<?php
get_header();
?>

<div id="middle">

<div id="main">
<h1>Search Results<?php if(isset($s))  echo " for &#147;" . get_search_query() . '&#148;'; ?></h1>


<?php


	if(have_posts()) :

	$odd = 'odd';

	$resCount = $wp_query->found_posts;
	echo '<h4 style="color:#dc7418;">' . $resCount . ' Website Results</h4>';

	while(have_posts()) : the_post();


	    ?>
	    <div class="resultRow <?php echo $odd; ?>">
	    <?php echo jp_get_featured_image($post->ID,'medium'); ?>
	    <p><b><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></b>
		&nbsp;&nbsp;&nbsp;<span style="color: #555;font-size:.8em;font-style:italic;"><?php if( $post->post_type == 'post' ) echo 'article'; else echo $post->post_type; ?></span>
	    <br>

	    <?php if($post->post_type == 'event') {

			$evdates = mejp_getEventDates();

			if(!empty($evdates)) {
				$evtime = mejp_getEventTime();

				echo '<i>';

				$evdatesStr = implode(', ',$evdates);

				echo $evdatesStr . '&mdash; ' .$evtime . '</i> <br>';


			}

	    } else if($post->post_type == 'post') {
			echo '<i>' . get_the_date() . '</i> &mdash; ';
		}
	    ?>

	    <?php
	    list($excerpt) = str_split(	get_the_excerpt() , 100 );
	    echo $excerpt;
	    if(strlen(get_the_excerpt() > 100))
		echo '...';
	    ?>
	    </p>
	    </div>

	    <?php
	    $odd = ($odd == 'odd') ? '' : 'odd';

	endwhile; 
	?>
		
	<div class="index-results-paging">
    	<?php previous_posts_link( '&laquo; Previous page' ); ?>
		<?php next_posts_link( 'Next page &raquo;' ); ?>
	</div>
	
	
	<?php
	else :

	    echo '<h4>No website search results</h4>';

	endif;

?>


</div>

    <?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>