<?php get_header(); ?>

<div id="middle">

    <div id="main" role="main" page="single">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>



	<h1><?php the_title(); ?></h1>

	<?php

    echo '<div id="contentWrap">';

	the_content(); ?>

    </div>

    <div class="meta">

	    <?php
			$cats = get_the_category();
			if(!empty($cats)) {
				echo '<p>See more in: ';
				foreach($cats as $c) {
					$cArr[] =  '<a href="'.get_category_link( $c->term_id ).'">'.$c->cat_name.'</a>';
				}
				$cStr = implode(' | ',$cArr);
				echo $cStr . '</p>';
			}
		?>
		<p>Written on: <?php the_date('M d, Y'); ?></p>

    </div>
    <?php endwhile; endif; ?>



    </div>


    <?php get_sidebar(); ?>


</div>

<?php get_footer(); ?>