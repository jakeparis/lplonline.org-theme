
<ul class="helpForm" class="sectionPageList">
	<li>All Newsletters</li>
    <?php wp_list_pages( array(
	'sort_column' => 'post_name',
	'child_of' => $post->post_parent,
	'title_li' => ''
	) );
    ?>
</ul>



<?php

    if(is_numeric($post->ID)) {
	$dontFeatureThisPage = "AND ".$wpdb->posts.".ID != '".$post->ID."'";
    }

    //*************** FEATURED PAGES ***********
    $featured = $wpdb->get_results($wpdb->prepare(
	"SELECT ".$wpdb->posts.".post_title as title,
	".$wpdb->posts.".ID,
	".$wpdb->posts.".post_content AS content,
	".$wpdb->posts.".guid AS permalink,
	".$wpdb->posts.".post_type,
	".$wpdb->posts.".post_date
	FROM ".$wpdb->postmeta.", ".$wpdb->posts."
	WHERE ".$wpdb->posts.".ID = ".$wpdb->postmeta.".post_id
	AND meta_key = 'feature page'
	AND ".$wpdb->posts.".post_status='Publish'
	$dontFeatureThisPage
	ORDER BY rand() LIMIT 2;"
	));

    if ($featured) : ?>

	<div id="suggestedContent">
	<h5>Other Interesting Pages</h5>

	<?php

	foreach($featured as $un) {
	    echo '<div class="suggestedSingle">

		<p>';
		if($thumb = jp_get_featured_image($un->ID,'thumb'))
		    echo '<a href="'.$un->permalink.'">'.$thumb.'</a> ';
		else
		    echo '<a href="'.$un->permalink.'"><img src="'.THEMEDIR.'/i/logoThumb.png"></a>';

		echo ' <a href="'.$un->permalink.'">'.$un->title.'</a>';

		if( $un->post_type == 'event')
		    echo ' &mdash; <b>' . getEventDateJp('M d, Y',$un) . '</b><br>';

		else if( $un->post_type == 'post')
		    echo '<br><i>' . date('M d, Y',strtotime($un->post_date)) . '</i> &mdash; ';

		else
		    echo '<br>';

		list($excerpt) = str_split( strip_tags($un->content), 150 );
		if(strlen($un->content) > 150)
		    $excerpt .= '...';

		echo $excerpt .'</p>
	    </div>';
	}

	echo '</div>';

    endif;
    ?>
