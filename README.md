# Lewiston Public Library

This theme was originally created in 2013 for the LPL in Lewiston, Maine.

## Changelog

### v2.2.3

Fixed sidebar layout and menu on various screen sizes.

### v2.2.2

* Fixed teen/kids footer caching issue.
* made footer email clickable.

### v2.2.0

Added updater code (gitlab tag deploy)

### 07-2021

Applied updates to theme colors, drop-shadows, bg patterns, etc. to 
match city's updated branding. 