<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Organization">

<head>
    <title>Chat with a Librarian // LPL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body style="background: #ddf2f6;padding:20px;">

<div id="chatWrap">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript" ></script>
    <div id="questionpoint.chatwidget" qwidgetno="1" ></div>
    <script id="questionpoint.bootstrap" qwidgetno="1" type="text/javascript" src="http://www.questionpoint.org/crs/qwidgetV4/js/qwidget.bootstrap.js?langcode=1&instid=13596&skin=black&size=fill" charset="utf-8">//<noscript>Please enable javascript to chat with librarians online</noscript></script>

<p>Feel free to call us at the Reference desk (<b>207-513-3135</b>) if you would like to speak with a person during business hours.<br>
<ul>
    <li>Mon&mdash;Thur: 10 - 7</li>
    <li>Fri&mdash;Sat: 10 - 5</li>
    <li>Summer Saturdays: 10 - 2</li>
</ul>


</div>

</body>
</html>