<?php
/*
Template Name: Teens--Readers Advisory Form
*/

if(isset($_POST['salutation'])) {

	if($_POST['salutation'] != '') return;

	$msg = '<div id="message">';

	$regName = strip_tags($_POST['regName']);
	$cardnum = strip_tags($_POST['regCardNumber']);
	$email = strip_tags($_POST['regEmail'],'limited');
	$email2 = strip_tags($_POST['regEmail2'],'limited');
	$comments = strip_tags($_POST['regComments']);
	$mailingList = (isset($_POST['mailingList'])) ? true : false;

	if (
		$regName == '' ||
		$cardnum == '' ||
		$comments == '' ||
		$email == ''
		) {
		$msg .= '<span class="error">You missed a required field.</span>';

	} else if(strpos($email,'@') == FALSE || strpos($email,'.') == FALSE) {
		// validate email input
		$msg .= '<span class="error">Email Address was not in the correct format.</span>';


	} else if ((!is_numeric($cardnum) || strlen($cardnum) != 14) && $cardnum != '') {
		$msg .= '<span class="error">Your library card was not entered correctly.</span>';

	} else if (strlen($comments) < 100) {
		$msg .= '<span class="error">You need to type at least 100 letters.</span>';

	} else if ($email != $email2) {
		$msg .= '<span class="error">The email addresses do not match.</span>';

	} else {
		// all checks out, do stuff

		$headers = array("Reply-To: $email");

		$body .= '
		    <p>Submission from Teen Readers Advisory Page.
		    <table>
			<tr>
			    <td>Name</td><td><b>' . $regName . '</b></td>
			</tr>
			<tr>
			    <td>Email</td><td><b>' . $email . '</b></td>
			</tr>
			<tr>
			    <td>Card</td><td><b>' . $cardnum . '</b></td>
			</tr>
			<tr>
			    <td>Description</td><td>' . $comments . '</td>
			</tr>
		    </table>';

		$result = wp_mail(FORMRECIPIENT,"Teen Readers Advisory Form ($regName)",$body,$headers);

		if($result) {
			$msg .= 'Your form was submitted. We will be contacting you shortly!';
			unset($regName,$comments,$email,$email2,$cardnum,$mailingList);
		} else {
			$msg .= 'Error! There was an unknown error sending your message. Please try again later.';
		}
	}
	$msg .= '</div>';

} // end form submitted

get_header('teens'); ?>

<div id="middle">

    <div id="main">

    <?php if (have_posts()) : while (have_posts()) : the_post();

    ?>


	<h1><?php the_title(); ?></h1>

	<?php if(isset($msg)) echo $msg; ?>

	<?php the_content(); ?>


	<script type="text/javascript">
	    jQuery(function($){

	    $("form#advisoryForm textarea").keyup(function(){
		var countem = $(this).val().length;
		if(countem < 100) {
			$("#minCharsCounter").css('color','#fe5c42').html("<b>" + countem + "</b> letters. You need at least 100.");
		} else if(countem < 180) {
			$("#minCharsCounter").css("color","#ffda44").html("<b>" + countem + "</b> letters. Okay, but keep writing!");
		} else if(countem >= 180) {
			$("#minCharsCounter").css("color","#7cdf07").html("<b>" + countem + "</b> letters. Excellent.");
		}

	    });

	    });
	</script>


	<form method="post" action="" id="advisoryForm" class="pageForm">
	    <input type="text" value="" id="salutation" name="salutation" />

	    <label class="required" for="regName">Name</label>
	    <input required="required" name="regName" type="text" value="<?php jpIfSet('regName'); ?>" />

	    <label class="required" for="regCardNumber">Card Number</label>
	    <input maxlength="14" name="regCardNumber" type="text" value="<?php jpIfSet('regCardNumber'); ?>" />

	    <label class="required"  for="regEmail">Email</label>
	    <input required="required" name="regEmail" type="email" value="<?php jpIfSet('regEmail'); ?>" />

	    <label for="regEmail2">confirm Email</label>
	    <input name="regEmail2" type="email" value="<?php jpIfSet('regEmail2'); ?>" />

	    <label for="regComments"><b>Please tell us</b> some of your favorites: books, authors, topics &mdash; even movies, if you aren't sure. Feel free to also tell us what types of books or authors you don't like. We'll use this information to help recommend some great books just for you.</label>

	    <div style="font-family:nobile,san-serif;font-size:.8em;background: #333;padding:4px;color: #fff;text-align:center;width:60%;"><b><span id="minCharsCounter">Minimum: 100 letters.</span></b></div>
	    <textarea name="regComments" style="height:280px;"><?php jpIfSet('regComments'); ?></textarea>

	    <input name="Submit" type="submit" value="Submit" />

	</form>


    <div class="meta">

	<p class="breadcrumbs"><b>You are Here:</b> &nbsp;<?php echo jp_breadcrumb(); ?></p>
	<p>Page last updated: <?php echo get_the_modified_date('M d, Y'); ?></p>

    </div>
    <?php endwhile; endif; ?>



    </div>


    <?php get_sidebar('teens'); ?>


</div>

<?php get_footer('teens'); ?>