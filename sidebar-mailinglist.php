
<form class="helpForm" method="post" action="http://lplonline.us6.list-manage2.com/subscribe/post">

    <h5>Signup for Our Mailing Lists</h5>
	<input type="hidden" name="u" value="20679f575e3f508fe2bc17335">
	<input type="hidden" name="id" value="dbbfa932a9">

	<label for="MERGE1">First Name</label> <input type="text" name="MERGE1" data-cookie="name" value="<?php echo $_COOKIE['patronName']; ?>">
	<label for="MERGE2">Last Name</label> <input type="text" name="MERGE2">
	<label for="MERGE0" required="required">Email</label> <input type="email" name="MERGE0" value="<?php echo $_COOKIE['patronName']; ?>">


	<label for="mailingListAdults"><input type="checkbox" name="group[153][1]" value="1" checked="checked" /> Adult Events</label>
	<label for="mailingListTeens"><input type="checkbox" name="group[153][2]" value="1" checked="checked" /> Teen Events</label>
	<label for="mailingListKids"><input type="checkbox" name="group[153][3]" value="1" checked="checked" /> Kids Events</label>

	<input type="submit" value="Subscribe to list" name="submit" />

</form>


<?php

    if(is_numeric($post->ID)) {
	$dontFeatureThisPage = "AND ".$wpdb->posts.".ID != '".$post->ID."'";
    }

    //*************** FEATURED PAGES ***********
    $featured = $wpdb->get_results(
	"SELECT ".$wpdb->posts.".post_title as title,
	".$wpdb->posts.".ID,
	".$wpdb->posts.".post_content AS content,
	".$wpdb->posts.".guid AS permalink,
	".$wpdb->posts.".post_type,
	".$wpdb->posts.".post_date
	FROM ".$wpdb->postmeta.", ".$wpdb->posts."
	WHERE ".$wpdb->posts.".ID = ".$wpdb->postmeta.".post_id
	AND meta_key = 'feature page'
	AND ".$wpdb->posts.".post_status='Publish'
	$dontFeatureThisPage
	ORDER BY rand() LIMIT 2;"
	);

    if ($featured) : ?>

	<div id="suggestedContent">
	<h5>Other Interesting Pages</h5>

	<?php

	foreach($featured as $un) {
	    echo '<div class="suggestedSingle">

		<p>';
		if($thumb = jp_get_featured_image($un->ID,'thumb'))
		    echo '<a href="'.$un->permalink.'">'.$thumb.'</a> ';
		else
		    echo '<a href="'.$un->permalink.'"><img src="'.THEMEDIR.'/i/logoThumb.png"></a>';

		echo ' <a href="'.$un->permalink.'">'.$un->title.'</a>';

		if( $un->post_type == 'event')
		    echo ' &mdash; <b>' . getEventDateJp('M d, Y',$un) . '</b><br>';

		else if( $un->post_type == 'post')
		    echo '<br><i>' . date('M d, Y',strtotime($un->post_date)) . '</i> &mdash; ';

		else
		    echo '<br>';

		list($excerpt) = str_split( strip_tags($un->content), 150 );
		if(strlen($un->content) > 150)
		    $excerpt .= '...';

		echo $excerpt .'</p>
	    </div>';
	}

	echo '</div>';

    endif;
    ?>
