<div role="complementary" id="sidebar">
<?php

 // USE THIS FOR EVENTS
$events = mejp_getEvents(3, 'future', 'teens');

if ($events) :

echo '<h2>Upcoming Teen Events</h2>';
$odd = '';
$shown = array();

foreach($events as $eventDate => $evsForDate) {
    foreach($evsForDate as $evOccurances) {
        foreach($evOccurances as $ev) {

            if( count($shown) > 2 )
                break 3;

            if($ev->eGroup == 1) {
            // don't show the event multiple times...
            if(in_array($ev->ID,$shown)) continue;
            }
            ?>

            <div class="singleEvent" style="padding-left:5px;">
            <h2><a href="<?php echo $ev->guid; ?>"><?php echo jp_get_featured_image($ev->ID) . $ev->post_title; ?></a></h3>
            <p class="eventDate"><?php echo date('M d, Y', strtotime($eventDate)) . ' &mdash; ' . $ev->time .'</p>';

            $showChars = 150;

            $desc = strip_tags($ev->post_content);
            list($desc) = str_split($desc,$showChars);
            echo '<p>' . $desc;
            if(strlen( $ev->post_content ) > $showChars)
                echo '...<a href="' . $ev->guid . '">read more</a>.';

            echo '</p>';
            ?>
            </div>


            <?php
            $odd = ($odd == 'odd') ? '' : 'odd';

            $shown[] = $ev->ID;
        }
    }
}

echo '<p style="background:transparent;text-align:center;"><b>See <a href="' . get_permalink(1162) . '">all upcoming events for teens</a>.</b></p>';

else : ?>

<p style="color: #bbb;text-align:right;font-style:italic;padding:15px 15px 0;">No events at the moment.</p>

<?php
/* 
// otherwise use this for tagcloud/etc
echo '
<div id="teenTagCloud">
<h5>Browse by Tag</h5>';
wp_tag_cloud('order=RAND');
echo '</div>';
?>

<form class="helpForm" id="helpFormSidebar" method="post" action="">
    <img class="ajaxLoader" src="<?PHP echo THEMEDIR; ?>/i/ajax-loader.gif">
    <input type="text" name="salutation" id="salutation" value="">
    <h5>Need Help? Ask Here.</h5>
    <p>We try to respond to everyone within a day.</p>
    <label>Name</label
    <label>Name</label>
    <input type="text" name="helpFormName" value="" class="ajaxFill">
    <label>Contact info (email or phone)</label>
    <input type="text" name="helpFormContact" value="" class="ajaxFill">
    <label>Question / Comment</label>
    <textarea name="helpFormQuestion"></textarea>
    <div id="helpFormMailingLists" style="color: #777;">
	<p><b>Signup for our Mailing Lists?</b></p>
	<label for="mailingListAdults"><input type="checkbox" name="mailingListAdults" value="1" checked="checked" /> Adult Events</label><br>
	<label for="mailingListTeens"><input type="checkbox" name="mailingListTeens" value="1" checked="checked" /> Teen Events</label><br>
	<label for="mailingListKids"><input type="checkbox" name="mailingListKids" value="1" checked="checked" /> Kids Events</label><br>
    </div>
    <input type="submit" value="Send Your Question" name="helpFormSubmit">

    <?php
    /* library chat splash 
    $path = str_replace(SITEHOME,'.',THEMEDIR) . '/i/librarians';

    if(!function_exists('glob')) { // available on php 5.3 & up, faster then directory iterator
	foreach (new DirectoryIterator($path) as $fileInfo) {
	    $fileName = $fileInfo->getFilename();
	    if($fileInfo->isDot() || $fileName == 'index.php') continue;
	    $imgArr[] = $fileName;
	}
    } else {
    foreach(glob($path . '/*.jpg') as $fileName)
	    $imgArr[] = basename( $fileName );
    }

    $randKey = array_rand($imgArr);

    $img = THEMEDIR . '/i/librarians/' . $imgArr[$randKey];

    ?>
    <a href="#" class="openChatWindow" id="chatSplash">
	<img style="width:200px;" src="<?php echo $img; ?>" alt="Ask Us - Chat Now"/>
    </a>

</form>

<?php // endif; /* USE FOR EVENTS */ 

endif;

?>

</div>